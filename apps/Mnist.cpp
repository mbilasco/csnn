#include "Experiment.h"
#include "dataset/Mnist.h"
#include "stdp/Multiplicative.h"
#include "stdp/Biological.h"
#include "stdp/Proportional.h"
#include "layer/Convolution.h"
#include "Distribution.h"
#include "execution/OptimizedLayerByLayer.h"
#include "analysis/Svm.h"
#include "analysis/Activity.h"
#include "analysis/Coherence.h"
#include "layer/Pooling.h"
#include "process/OnOffFilter.h"
#include "process/Scaling.h"
#include "process/Pooling.h"
#include "stdp/Linear.h"
#include "stdp/BiologicalMultiplicative.h"
#include "analysis/SaveOutput.h"

int main(int argc, char** argv) {

	//passing the name of the experiment "mnist" along with the arguments.
	Experiment<OptimizedLayerByLayer> experiment(argc, argv, "mnist");

	//choosing the On-Off filter used, egde detection for feature extraction. (filter_size, center_dev, surround_dev)
	experiment.template add_preprocessing<process::DefaultOnOffFilter>(7, 1.0, 4.0);
	//feature extraction, edge-curve-color.
	experiment.template add_preprocessing<process::FeatureScaling>();
	//temporal coding used, the data value "float" is transformed and reprisented by the spike timestamp.
	experiment.template input<LatencyCoding>();

    //get the input path of the dataset.
	const char* input_path_ptr = std::getenv("INPUT_PATH");

	//if the input path doesn't exist, return an error message, else continue normally.
	if(input_path_ptr == nullptr) {
		throw std::runtime_error("Require to define INPUT_PATH variable");
	}

	//put the filepath in a string variable called input_path
	std::string input_path(input_path_ptr);
	//adding the train and test datasets.
	experiment.template add_train<dataset::Mnist>(input_path+"train-images.idx3-ubyte", input_path+"train-labels.idx1-ubyte");
	experiment.template add_test<dataset::Mnist>(input_path+"t10k-images.idx3-ubyte", input_path+"t10k-labels.idx1-ubyte");
	//synaptic weights learning rate
	float w_lr = 0.1f;
	//threshould learning rate
	float th_lr = 1.0f;
	//t expected, time at which a neuron is expected to fire.
	float t_obj = 0.75f; //0.75 gives state of the art results in Mnist.

	//first convolution layer, name, width, hight, depth of filter(NUMBER OF FEATURES nf - filter number).
	//stride x and y are fixed to 1 which means there is a big overlapping while extracting features.
	//for each 5 by 5 input theres 64 filters (neurons).
	auto& conv1 = experiment.template push_layer<layer::Convolution>("conv1", 5, 5, 32);
	conv1.template parameter<float>("annealing").set(0.95f);
	conv1.template parameter<float>("min_th").set(1.0f);
	conv1.template parameter<float>("t_obj").set(t_obj);
	conv1.template parameter<float>("lr_th").set(th_lr);
	conv1.template parameter<Tensor<float>>("w").template distribution<distribution::Uniform>(0.0, 1.0);
	conv1.template parameter<Tensor<float>>("th").template distribution<distribution::Gaussian>(8.0, 0.1);
	conv1.template parameter<STDP>("stdp").template set<stdp::Biological>(w_lr, 0.1f);

	//A 2x2 filter with a stride of 2, this reduces our input by a factor of 4. 
	//we take for example 4 outputs of 4 neurons and make them 1 output the max of sum avg.
	experiment.template push_layer<layer::Pooling>("pool1", 2, 2, 2, 2);

	//why did he use more filters though we have less inputs?
	//combine features into 1 feature (high level features).
	auto& conv2 = experiment.template push_layer<layer::Convolution>("conv2", 5, 5, 64);//128 number of filter.
	conv2.template parameter<float>("annealing").set(0.95f);
	conv2.template parameter<float>("min_th").set(1.0f);
	conv2.template parameter<float>("t_obj").set(t_obj);
	conv2.template parameter<float>("lr_th").set(th_lr);
	conv2.template parameter<Tensor<float>>("w").template distribution<distribution::Uniform>(0.0, 1.0);
	conv2.template parameter<Tensor<float>>("th").template distribution<distribution::Gaussian>(10.0, 0.1);
	conv2.template parameter<STDP>("stdp").template set<stdp::Biological>(w_lr, 0.1f);

	//another pooling for the same purpose. after this layer all values become a linear vector to enter the fully connected layer.
	experiment.template push_layer<layer::Pooling>("pool2", 2, 2, 2, 2);

	//the training of this layer combines all features to come up with a result 
	//(in case of Mnist this should be 10 output neurons).
	auto& fc1 = experiment.template push_layer<layer::Convolution>("fc1", 4, 4, 64);
	fc1.template parameter<float>("annealing").set(0.95f);
	fc1.template parameter<float>("min_th").set(1.0f);
	fc1.template parameter<float>("t_obj").set(t_obj);
	fc1.template parameter<float>("lr_th").set(th_lr);
	fc1.template parameter<Tensor<float>>("w").template distribution<distribution::Uniform>(0.0, 1.0);
	fc1.template parameter<Tensor<float>>("th").template distribution<distribution::Gaussian>(10.0, 0.1);
	fc1.template parameter<STDP>("stdp").template set<stdp::Biological>(w_lr, 0.1f);

	//training the 2 conv layers and the fully connected layer each for 100 epochs one by one.
	experiment.add_train_step(conv1, 100); 
	experiment.add_train_step(conv2, 100);
	experiment.add_train_step(fc1, 100);

	conv1.plot_threshold(true);
	conv1.plot_reconstruction(true);
	
	/**
	 * TimeObjectiveOutput reconstructs the signal.
	*/
	auto& conv1_out = experiment.template output<TimeObjectiveOutput>(conv1, t_obj);
	//why 2,2 not 1,1 if for flattening before SVM?
	//to get 4 feature vectors each repreeesenting 1/4 the image and then they are concatinated to form 1 flattened vector.
	conv1_out.template add_postprocessing<process::SumPooling>(2, 2); 

	conv1_out.template add_postprocessing<process::FeatureScaling>();
	conv1_out.template add_analysis<analysis::Activity>();
	conv1_out.template add_analysis<analysis::Coherence>();
	conv1_out.template add_analysis<analysis::Svm>();

	auto& conv2_out = experiment.template output<TimeObjectiveOutput>(conv2, t_obj);
	//to get 4 feature vectors each repreeesenting 1/4 the image and then they are concatinated to form 1 flattened vector.
	conv2_out.template add_postprocessing<process::SumPooling>(2, 2);
	conv2_out.template add_postprocessing<process::FeatureScaling>();
	conv2_out.template add_analysis<analysis::Activity>();
	conv2_out.template add_analysis<analysis::Coherence>();
	conv2_out.template add_analysis<analysis::Svm>();

	//fully connected layer.
	auto& fc1_out = experiment.template output<TimeObjectiveOutput>(fc1, t_obj);
	fc1_out.template add_postprocessing<process::FeatureScaling>();
	//fc1_out.template add_analysis<process::SumPooling>(1, 1);
	fc1_out.template add_analysis<analysis::Activity>();
	//fc1_out.template add_analysis<analysis::Coherence>();
	fc1_out.template add_analysis<analysis::Svm>();
	
	experiment.run(1000);
	return experiment.wait();

}

//use 3 pics as 1 input to check the different features.