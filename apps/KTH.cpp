#include "Experiment.h"
#include "dataset/KTH.h"
#include "stdp/Multiplicative.h"
#include "stdp/Biological.h"
#include "layer/SpatioTemporalConvolution.h"
#include "layer/Pooling.h"
#include "Distribution.h"
#include "execution/OptimizedLayerByLayer.h"
#include "analysis/Svm.h"
#include "analysis/Activity.h"
#include "analysis/Coherence.h"
#include "process/Input.h"
#include "process/Scaling.h"
#include "process/Pooling.h"
#include "process/OnOffFilter.h"

int main(int argc, char** argv) {
	//declaaring an object of type experiment.
	Experiment<OptimizedLayerByLayer> experiment(argc, argv, "KTH");

    //get the input path of the dataset.
	const char* input_path_ptr = std::getenv("INPUT_PATH");

	//if the input path doesn't exist, return an error message, else continue normally.
	if(input_path_ptr == nullptr) {
		throw std::runtime_error("Require to define INPUT_PATH variable");
	}

	//put the filepath in a string variable called input_path
	std::string input_path(input_path_ptr);
	
	//transfering the input sample into spikes.
	experiment.input<LatencyCoding>();

	//adding the train datasets.
	experiment.add_train<dataset::KTH>(std::vector<std::string>({input_path+"Dataset_Train01.bin"}));

	//adding the test datasets.
	experiment.add_test<dataset::KTH>(std::vector<std::string>({input_path+"Dataset_Test01.bin"}));

	//t expected for the neuron to fire.
	float t_obj = 0.95f;
	//learning rate of the neuron threashould.
	float th_lr = 1.0f;
	//learning rate of the synaptic weights.
	float w_lr = 0.1f;

	//this is one convolutional layer that extracts low level features. check layer/convolution.cpp for details on each parameter.
	auto& conv1 = experiment.push_layer<layer::STConvolution>("conv1", 5, 5, 32); 
	conv1.parameter<float>("annealing").set(0.95f);
	conv1.parameter<float>("min_th").set(4.0f);
	conv1.parameter<float>("t_obj").set(t_obj);
	conv1.parameter<float>("lr_th").set(th_lr);
	conv1.parameter<Tensor<float>>("w").distribution<distribution::Uniform>(0.0, 1.0);
	conv1.parameter<Tensor<float>>("th").distribution<distribution::Gaussian>(5.0, 0.1);
	conv1.parameter<STDP>("stdp").set<stdp::Biological>(w_lr, 0.1);

	//training the neurons on 100 epochs.
	experiment.add_train_step(conv1, 100);

	auto& conv1_out = experiment.output<TimeObjectiveOutput>(conv1, t_obj);
	conv1_out.add_postprocessing<process::SumPooling>(2, 2);
	conv1_out.add_postprocessing<process::FeatureScaling>();
	conv1_out.add_analysis<analysis::Activity>();
	conv1_out.add_analysis<analysis::Coherence>();
	conv1_out.add_analysis<analysis::Svm>();

	experiment.run(10000);
}