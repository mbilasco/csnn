#include "Experiment.h"
#include "dataset/STL.h"
#include "stdp/Multiplicative.h"
#include "stdp/Biological.h"
#include "layer/Convolution.h"
#include "layer/Pooling.h"
#include "Distribution.h"
#include "execution/OptimizedLayerByLayer.h"
#include "analysis/Svm.h"
#include "analysis/Activity.h"
#include "analysis/Coherence.h"
#include "process/Input.h"
#include "process/Scaling.h"
#include "process/Pooling.h"
#include "process/OnOffFilter.h"

int main(int argc, char** argv) {
	//create an object of type experiment.
	Experiment<OptimizedLayerByLayer> experiment(argc, argv, "stl_10_1");

	//get the input path of the class as chars, this path is needed below to choose the training sets in their file locations
	const char* input_path_ptr = std::getenv("INPUT_PATH");

	//if the input path doesn't exist, return an error message, else continue normally.
	if(input_path_ptr == nullptr) {
		throw std::runtime_error("Require to define INPUT_PATH variable");
	}

	//put the filepath in a string variable called input_path
	std::string input_path(input_path_ptr);

	//some pre-processing for the data before using them in the network.
	experiment.add_preprocessing<process::BiologicalOnOffFilter>(7, 1.0, 4.0);
	//changing the size of the input sample, feature extraction, edge-curve-color.
	experiment.add_preprocessing<process::FeatureScaling>();
	//temporal coding used, the data value is reprisented by the spike timestamp.
	experiment.input<LatencyCoding>();

	//adding the train and test datasets.
	experiment.add_train<dataset::STL>(input_path+"train_X.bin", input_path+"train_y.bin");
	experiment.add_test<dataset::STL>(input_path+"test_X.bin", input_path+"test_y.bin");

	//t expected, time at which a neuron is expected to fire.
	float t_obj = 0.85f; 
	//threshould learning rate
	float th_lr = 1.0f;
	//synaptic weights learning rate
	float w_lr = 0.1f;

	//first convolutional layer, typically extracts low level features. for some reason only 8 filters are used
	auto& conv1 = experiment.push_layer<layer::Convolution>("conv1", 5, 5, 128);
	conv1.parameter<float>("annealing").set(0.95f);
	conv1.parameter<float>("min_th").set(4.0f);
	conv1.parameter<float>("t_obj").set(t_obj);
	conv1.parameter<float>("lr_th").set(th_lr);
	conv1.parameter<Tensor<float>>("w").distribution<distribution::Uniform>(0.0, 1.0);
	conv1.parameter<Tensor<float>>("th").distribution<distribution::Gaussian>(5.0, 0.1);
	conv1.parameter<STDP>("stdp").set<stdp::Biological>(w_lr, 0.1);

	experiment.add_train_step(conv1, 100);

	auto& conv1_out = experiment.output<TimeObjectiveOutput>(conv1, t_obj);
	conv1_out.add_postprocessing<process::SumPooling>(2, 2);
	conv1_out.add_postprocessing<process::FeatureScaling>();
	conv1_out.add_analysis<analysis::Activity>();
	conv1_out.add_analysis<analysis::Coherence>();
	conv1_out.add_analysis<analysis::Svm>();


	//conv1.plot_threshold(true);
	//conv1.plot_reconstruction<process::GaussianTemporalCodingColor>(true);

	experiment.run(10000);
}
