#include "Benchmark.h"
#include "process/OnOffFilter.h"
#include "process/Scaling.h"
#include "process/Pooling.h"
#include "layer/Convolution.h"
#include "layer/Pooling.h"
#include "stdp/Linear.h"
#include "analysis/Activity.h"
#include "analysis/Coherence.h"
#include "analysis/Svm.h"
#include "dataset/FaceMotor.h"
#include "execution/OptimizedLayerByLayer.h"
#include "stdp/BiologicalMultiplicative.h"

template<typename T>
struct Handler {

	/**
	 * function run that takes 2 parameters, experament and variables.
	 * @param experiment an object of type experiment, this class allows the user to add some pre-processing stages for the experiment,
	 *  such as choosing the OnOffFilter, adding FeatureScaling etc.. in addition to choosing the experiment coding in the input layer,
	 *  training and testing the datasets, and adding convolution layers, pooling layers and fully connected layers etc...
   	 * @param variables this is a mapped vector that allows adding dynamic objects to the experiment such as Time objects.
	 * @return no return (type void)
	 */

	static void run(Experiment<T>& experiment, const std::map<std::string, float>& variables) {
		//initialising the OnOffFilter used in this experiment with values for size_t filter_size, float center_dev, float surround_dev
		//on/off filtering is a kind of preprocessing which enables SNNs to learn from RGB images, use DoG filters to detect the edges, gives best results using black and white
		experiment.template add_preprocessing<process::DefaultOnOffFilter>(7, 1.0, 4.0);
		//feature extraction, edge-curve-color.
		experiment.template add_preprocessing<process::FeatureScaling>();
		//choosing the suitable converter for the input layer as a latency coding input converter, and initialising this layer.
		//an input converter should be defined, in order to transform the input tensor into spikes (earlier spikes encode higher values, while later spikes represent lower values)
		experiment.template input<LatencyCoding>();

		//get the input path of the class as chars, this path is needed below to choose the training sets in their file locations
		const char* input_path_ptr = std::getenv("INPUT_PATH");
		//if the input path doesn't exist, return an error message, else continue normally.
		if(input_path_ptr == nullptr) {
			throw std::runtime_error("Require to define INPUT_PATH variable");
		}
		//put the filepath in a string variable called input_path
		std::string input_path(input_path_ptr);


		//getting the data sets of faces, training and testing them
		experiment.template add_train<dataset::FaceMotor>(input_path+"TrainingSet/Face", input_path+"TrainingSet/Motor");
		experiment.template add_test<dataset::FaceMotor>(input_path+"TestingSet/Face", input_path+"TestingSet/Motor");

		//"t_obj" is a Time object - this is (texpected).
		//variables is a vector that contains variable names and their corresponding values.
		float t_obj = variables.at("t_obj");
		//the learning rate.
		float lr = 0.1f;

		// conv1 conv2 and fc1 are 3 different layers and there are 2 pooling layers so we have (5 layers in total).

		//push_layer is a function found in "Experiment.h" this function takes the layer name as string, and the layer args
		//the args passed are in fact (size_t filter_width, size_t filter_height, size_t filter_number, size_t stride_x, size_t stride_y, size_t padding_x, size_t padding_y)
		//this function is found in include/layer/convolution.cpp
		auto& conv1 = experiment.template push_layer<layer::Convolution>("conv1", 5, 5, 32, 1, 1, 5/2, 5/2);
		//setting the parameters found in convolution.cpp,
		//check include/layer/convolution.cpp for details on each parameter.
		conv1.template parameter<float>("annealing").set(0.95f);
		conv1.template parameter<float>("min_th").set(8.0f);
		conv1.template parameter<float>("t_obj").set(t_obj);
		conv1.template parameter<float>("lr_th").set(lr*10.0f);
		conv1.template parameter<Tensor<float>>("w").template distribution<distribution::Gaussian>(0.8, 0.01);
		conv1.template parameter<Tensor<float>>("th").template distribution<distribution::Gaussian>(5.0, 1.0);
		conv1.template parameter<STDP>("stdp").template set<stdp::Linear>(lr, lr);

		//to reduce the dimensionality of the data across the layers (pooling is shrinking an image to reduce its pixel density)
		//the arguments filled are Pooling(filter_width, filter_height, stride_x, stride_y, padding_x = 0, padding_y = 0);
		//where stride is the x and y movement of the kernel, and padding is extra 0 pixels addeed to the image.
		experiment.template push_layer<layer::Pooling>("pool1", 7, 7, 6, 6, 7/2, 7/2);

		auto& conv2 = experiment.template push_layer<layer::Convolution>("conv2", 17, 17, 64, 1, 1, 17/2, 17/2);
		conv2.template parameter<float>("annealing").set(0.95f);
		conv2.template parameter<float>("min_th").set(1.0f);
		conv2.template parameter<float>("t_obj").set(t_obj);
		conv2.template parameter<float>("lr_th").set(lr*10.0f);
		conv2.template parameter<Tensor<float>>("w").template distribution<distribution::Uniform>(0.0, 1.0);
		conv2.template parameter<Tensor<float>>("th").template distribution<distribution::Gaussian>(5.0, 1.0);
		conv2.template parameter<STDP>("stdp").template set<stdp::Linear>(lr, lr);

		//adding one more pooling layer
		experiment.template push_layer<layer::Pooling>("pool2", 5, 5, 5, 5, 5/2, 5/2);

		//this is a fully connected layer (dense layers),neurons of a layer l(i) are connected to all the neurons of layer l(i+1) .
		auto& fc1 = experiment.template push_layer<layer::Convolution>("fc1", 5, 5, 128, 1, 1, 5/2, 5/2);
		fc1.template parameter<float>("annealing").set(0.95f);
		fc1.template parameter<float>("min_th").set(1.0f);
		fc1.template parameter<float>("t_obj").set(t_obj);
		fc1.template parameter<float>("lr_th").set(lr*10.0f);
		fc1.template parameter<Tensor<float>>("w").template distribution<distribution::Uniform>(0.0, 1.0);
		fc1.template parameter<Tensor<float>>("th").template distribution<distribution::Gaussian>(5.0, 1.0);
		fc1.template parameter<STDP>("stdp").template set<stdp::Linear>(lr, lr);

		//after creating the 3 layers they are given the same train step which is the epoch_number
		experiment.add_train_step(conv1, 100);
		experiment.add_train_step(conv2, 100);
		experiment.add_train_step(fc1, 100);

		//Getting the output of each layer by passing in the layer and a time object.
		//Each output consists in an output converter (to transform back spikes into tensors)
		auto& conv1_out = experiment.template output<TimeObjectiveOutput>(conv1, t_obj);
		conv1_out.template add_postprocessing<process::SumPooling>(1, 1); //flattening thats why its 1,1
		conv1_out.template add_postprocessing<process::FeatureScaling>();
		//getting sparsity metrics
		conv1_out.template add_analysis<analysis::Activity>();
		//getting coherence metrics
		conv1_out.template add_analysis<analysis::Coherence>();
		//get the recognition rate obtained after classification by an SVM.
		conv1_out.template add_analysis<analysis::Svm>();

		auto& conv2_out = experiment.template output<TimeObjectiveOutput>(conv2, t_obj);
		conv2_out.template add_postprocessing<process::SumPooling>(1, 1);
		conv2_out.template add_postprocessing<process::FeatureScaling>();
		conv2_out.template add_analysis<analysis::Activity>();
		conv2_out.template add_analysis<analysis::Coherence>();
		conv2_out.template add_analysis<analysis::Svm>();

		//this is a fully connected layer (dense layers),neurons of a layer l(i) are connected to all the neurons of layer l(i+1) .
		auto& fc1_out = experiment.template output<TimeObjectiveOutput>(fc1, t_obj);
		fc1_out.template add_postprocessing<process::SumPooling>(1, 1);
		fc1_out.template add_postprocessing<process::FeatureScaling>();
		//getting sparsity metrics
		fc1_out.template add_analysis<analysis::Activity>();
		//getting coherence metrics
		fc1_out.template add_analysis<analysis::Coherence>();
		//get the recognition rate obtained after classification by an SVM (support vector machine).
		fc1_out.template add_analysis<analysis::Svm>();

	}
};
/**
 *
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char** argv) {
  //argv is an array of c-string pointers.
	//argc specifies the number of elements within argv or the number of c-string pointers pointed to by argv
	Benchmark<Handler, OptimizedLayerByLayer> bench(argc, argv, "bench-face_motor-1", 10);
	//adding a float vector with the name of "t_obj"
	bench.add_variable("t_obj", {0.2f, 0.3f, 0.4f,  0.5f, 0.6f, 0.7f, 0.8f, 0.9f});
	//the number passed in the run function is the thread_number
	bench.run(4);
}
