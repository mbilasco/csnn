#include "Experiment.h"
#include "dataset/Mnist.h"
#include "stdp/Multiplicative.h"
#include "stdp/Biological.h"
#include "stdp/Proportional.h"
#include "layer/Convolution.h"
#include "Distribution.h"
#include "execution/OptimizedLayerByLayer.h"
#include "analysis/Svm.h"
#include "analysis/Activity.h"
#include "analysis/Coherence.h"
#include "layer/Pooling.h"
#include "process/OnOffFilter.h"
#include "process/Scaling.h"
#include "process/Pooling.h"
#include "stdp/Linear.h"
#include "stdp/BiologicalMultiplicative.h"
#include "analysis/SaveOutput.h"

int main(int argc, char** argv) {

	Experiment<OptimizedLayerByLayer> experiment(argc, argv, "mnist100");										//Declaration of variable experiment

	int seed=atoi(argv[1]);
	float t_obj = atof(argv[2]);//0.75f);
	int nb_filters_conv_1=atoi(argv[3]);

	experiment.set_random_seed(seed);

	//separates into 2 channels (positive / negative)
	experiment.template add_preprocessing<process::DefaultOnOffFilter>(7, 1.0, 4.0);			//DoG + sépare en 2 canaux (positif/négatif) 

	experiment.template add_preprocessing<process::FeatureScaling>();											//met à l'échelle ?????????????????????? //scales
	//convert values to spikes (+ value ~ + tot)
	experiment.template input<LatencyCoding>();																						//convertie les valeurs en spikes (+valeur ~ +tot)

	const char* input_path_ptr = std::getenv("INPUT_PATH");

	if(input_path_ptr == nullptr) {
		throw std::runtime_error("Require to define INPUT_PATH variable");
	}

	std::string input_path(input_path_ptr);

	experiment.template add_train<dataset::Mnist>(input_path+"train-images.idx3-ubyte", input_path+"train-labels.idx1-ubyte"); 	/*ajout des datas
																																																															 d'entrainements */
	experiment.template add_test<dataset::Mnist>(input_path+"t10k-images.idx3-ubyte", input_path+"t10k-labels.idx1-ubyte");			/*ajout des datas
																																																															 de tests */
	//synaptic weigbts learning rate.
	float w_lr = 0.1f;
	//neuron threshould
	float th_lr = 1.0f;
	//float t_obj = 0.75f;

	auto& conv1 = experiment.template push_layer<layer::Convolution>("conv1", 5, 5, 32);
	conv1.template parameter<float>("annealing").set(0.95f);
	conv1.template parameter<float>("min_th").set(1.0f);
	conv1.template parameter<float>("t_obj").set(t_obj);
	conv1.template parameter<float>("lr_th").set(th_lr);
	conv1.template parameter<Tensor<float>>("w").template distribution<distribution::Uniform>(0.0, 1.0);
	conv1.template parameter<Tensor<float>>("th").template distribution<distribution::Gaussian>(8.0, 0.1);
	conv1.template parameter<STDP>("stdp").template set<stdp::Biological>(w_lr, 0.1f);											//STDP biologique

	experiment.template push_layer<layer::Pooling>("pool1", 2, 2, 2, 2);								//Pooling(fenetre 2*2 ; pas de 2)

	auto& conv2 = experiment.template push_layer<layer::Convolution>("conv2", 5, 5, 128);	//Second layer of convolution 16 filtres de 5*5
	conv2.template parameter<float>("annealing").set(0.95f);															//Defining parameters of the convolution 2
	conv2.template parameter<float>("min_th").set(1.0f);
	conv2.template parameter<float>("t_obj").set(t_obj);
	conv2.template parameter<float>("lr_th").set(th_lr);
	conv2.template parameter<Tensor<float>>("w").template distribution<distribution::Uniform>(0.0, 1.0);
	conv2.template parameter<Tensor<float>>("th").template distribution<distribution::Gaussian>(10.0, 0.1);
	conv2.template parameter<STDP>("stdp").template set<stdp::Biological>(w_lr, 0.1f);											//Choice of the STDP

	experiment.template push_layer<layer::Pooling>("pool2", 2, 2, 2, 2);								//Pooling(fenetre 2*2 ; pas de 2)

	auto& fc1 = experiment.template push_layer<layer::Convolution>("fc1", 4, 4, 4096);											//Final convolution 1024 filtres de 4*4
	fc1.template parameter<float>("annealing").set(0.95f);																									//Defining parameters of final convolution
	fc1.template parameter<float>("min_th").set(1.0f);
	fc1.template parameter<float>("t_obj").set(t_obj);
	fc1.template parameter<float>("lr_th").set(th_lr);
	fc1.template parameter<Tensor<float>>("w").template distribution<distribution::Uniform>(0.0, 1.0);
	fc1.template parameter<Tensor<float>>("th").template distribution<distribution::Gaussian>(10.0, 0.1);
	fc1.template parameter<STDP>("stdp").template set<stdp::Biological>(w_lr, 0.1f);												//Choice of the STDP

	experiment.add_train_step(conv1,100);
	experiment.add_train_step(conv2, 100);
	experiment.add_train_step(fc1, 100);

	//conv1.plot_threshold(false);
	//conv1.plot_reconstruction(false);

	auto& conv1_out = experiment.template output<TimeObjectiveOutput>(conv1, t_obj);
	conv1_out.template add_postprocessing<process::SumPooling>(2, 2);
	conv1_out.template add_postprocessing<process::FeatureScaling>();
	conv1_out.template add_analysis<analysis::Activity>();
	conv1_out.template add_analysis<analysis::Coherence>();
	conv1_out.template add_analysis<analysis::Svm>();

	auto& conv2_out = experiment.template output<TimeObjectiveOutput>(conv2, t_obj);
	conv2_out.template add_postprocessing<process::SumPooling>(2, 2);
	conv2_out.template add_postprocessing<process::FeatureScaling>();
	conv2_out.template add_analysis<analysis::Activity>();
	conv2_out.template add_analysis<analysis::Coherence>();
	conv2_out.template add_analysis<analysis::Svm>();

	auto& fc1_out = experiment.template output<TimeObjectiveOutput>(fc1, t_obj);
	fc1_out.template add_postprocessing<process::FeatureScaling>();
	fc1_out.template add_analysis<analysis::Activity>();
	fc1_out.template add_analysis<analysis::Coherence>();
	fc1_out.template add_analysis<analysis::Svm>();

	experiment.run(10000);

	return experiment.wait();

}
