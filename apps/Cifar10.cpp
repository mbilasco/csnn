#include "Experiment.h"
#include "dataset/Cifar.h"
#include "stdp/Multiplicative.h"
#include "stdp/Biological.h"
#include "layer/Convolution.h"
#include "layer/Pooling.h"
#include "Distribution.h"
#include "execution/OptimizedLayerByLayer.h"
#include "analysis/Svm.h"
#include "analysis/Activity.h"
#include "analysis/Coherence.h"
#include "process/Input.h"
#include "process/Scaling.h"
#include "process/Pooling.h"
#include "process/OnOffFilter.h"

int main(int argc, char** argv) {
	//declaaring an object of type experiment.
	Experiment<OptimizedLayerByLayer> experiment(argc, argv, "cifar10");

    //get the input path of the dataset.
	const char* input_path_ptr = std::getenv("INPUT_PATH");

	//if the input path doesn't exist, return an error message, else continue normally.
	if(input_path_ptr == nullptr) {
		throw std::runtime_error("Require to define INPUT_PATH variable");
	}

	//put the filepath in a string variable called input_path
	std::string input_path(input_path_ptr);

	//choosing the On-Off filter used, egde detection for feature extraction. (filter_size, center_dev, surround_dev)
	experiment.add_preprocessing<process::BiologicalOnOffFilter>(7, 1.0, 4.0);
	//chaanging the size of the input sample.
	experiment.add_preprocessing<process::FeatureScaling>();
	//transfering the input sample into spikes.
	experiment.input<LatencyCoding>();

	//adding the train datasets.
	experiment.add_train<dataset::Cifar>(std::vector<std::string>({input_path+"Boxing_DataTrain.bin"}));
	//experiment.add_train<dataset::Cifar>(std::vector<std::string>({input_path+"data_batch_1.bin"}));

	//adding the test datasets.
	experiment.add_test<dataset::Cifar>(std::vector<std::string>({input_path+"Boxing_DataTest.bin"}));
	//experiment.add_test<dataset::Cifar>(std::vector<std::string>({input_path+"data_batch_1.bin"}));

	//t expeected for the neuron to fire.
	float t_obj = 0.95f;
	//learning rate of the neuron threashould.
	float th_lr = 1.0f;
	//learning rate of the synaptic weights.
	float w_lr = 0.1f;

	//this is a one layer neural network in this example, check layer/convolution.cpp for details on each parameter.
	//this is a convolution layer that extracts low level features.
	auto& conv1 = experiment.push_layer<layer::Convolution>("conv1", 5, 5, 64);
	conv1.parameter<float>("annealing").set(0.95f);
	conv1.parameter<float>("min_th").set(4.0f);
	conv1.parameter<float>("t_obj").set(t_obj);
	conv1.parameter<float>("lr_th").set(th_lr);
	conv1.parameter<Tensor<float>>("w").distribution<distribution::Uniform>(0.0, 1.0);
	conv1.parameter<Tensor<float>>("th").distribution<distribution::Gaussian>(5.0, 0.1);
	conv1.parameter<STDP>("stdp").set<stdp::Biological>(w_lr, 0.1);

	//training the neurons on 100 epochs.
	experiment.add_train_step(conv1, 1000);

	auto& conv1_out = experiment.output<TimeObjectiveOutput>(conv1, t_obj);
	conv1_out.add_postprocessing<process::SumPooling>(2, 2);
	conv1_out.add_postprocessing<process::FeatureScaling>();
	conv1_out.add_analysis<analysis::Activity>();
	conv1_out.add_analysis<analysis::Coherence>();
	conv1_out.add_analysis<analysis::Svm>();

	//conv1.plot_threshold(true);
	//conv1.plot_reconstruction<process::GaussianTemporalCodingColor>(true);

	experiment.run(10000);
}
