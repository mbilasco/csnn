#include "Experiment.h"
#include "dataset/Cifar.h"
#include "stdp/Multiplicative.h"
#include "stdp/Biological.h"
#include "layer/Convolution.h"
#include "layer/Pooling.h"
#include "Distribution.h"
#include "execution/OptimizedLayerByLayer.h"
#include "analysis/Svm.h"
#include "analysis/Activity.h"
#include "analysis/Coherence.h"
#include "process/Input.h"
#include "process/Scaling.h"
#include "process/Pooling.h"
#include "process/OnOffFilter.h"

int main(int argc, char** argv) {
	//declaaring an object of type experiment.
	Experiment<OptimizedLayerByLayer> experiment(argc, argv, "cifar10");

    //get the input path of the dataset.
	const char* input_path_ptr = std::getenv("INPUT_PATH");

	//if the input path doesn't exist, return an error message, else continue normally.
	if(input_path_ptr == nullptr) {
		throw std::runtime_error("Require to define INPUT_PATH variable");
	}

	//put the filepath in a string variable called input_path
	std::string input_path(input_path_ptr);

	//choosing the On-Off filter used, egde detection for feature extraction. (filter_size, center_dev, surround_dev)
	experiment.add_preprocessing<process::BiologicalOnOffFilter>(7, 1.0, 4.0);
	//chaanging the size of the input sample.
	experiment.add_preprocessing<process::FeatureScaling>();
	//transfering the input sample into spikes.
	experiment.input<LatencyCoding>();

	//adding the train datasets.
	experiment.add_train<dataset::Cifar>(std::vector<std::string>({
		input_path+"data_batch_1.bin",
		input_path+"data_batch_2.bin",
		input_path+"data_batch_3.bin",
		input_path+"data_batch_4.bin",
		input_path+"data_batch_5.bin"
	}));

	//adding the test datasets.
	experiment.add_test<dataset::Cifar>(std::vector<std::string>({
		input_path+"test_batch.bin"
	}));

	//t expeected for the neuron to fire.
	float t_obj = 0.95f;
	//learning rate of the neuron threashould.
	float th_lr = 1.0f;
	//learning rate of the synaptic weights.
	float w_lr = 0.1f;

	//first convolutional layer, typically extracts low level features. for some reason only 8 filters are used
	//8 filters means 8 features can be detected.
	auto& conv1 = experiment.push_layer<layer::Convolution>("conv1", 5, 5, 8);
	conv1.parameter<float>("annealing").set(0.95f); //this factor helps update other factores, such as the alfa parameter in the STDP
	conv1.parameter<float>("min_th").set(4.0f); //minimum threashould of the neurons.
	conv1.parameter<float>("t_obj").set(t_obj); //time at witch a neuron is expected to fire.
	conv1.parameter<float>("lr_th").set(th_lr);
	conv1.parameter<Tensor<float>>("w").distribution<distribution::Uniform>(0.0, 1.0);
	conv1.parameter<Tensor<float>>("th").distribution<distribution::Gaussian>(5.0, 0.1);
	conv1.parameter<STDP>("stdp").set<stdp::Biological>(w_lr, 0.1);//learning rule, spike time dependant plasticity.

	//a pooling layer with a factor of 2x2xnf number of features.
	experiment.push_layer<layer::Pooling>("pool1", 2, 2, 2, 2);

	//second convolutional layer that is supposed to get more suffisticated features.
	auto& conv2 = experiment.push_layer<layer::Convolution>("conv2", 5, 5, 16);
	conv2.parameter<float>("annealing").set(0.95f);
	conv2.parameter<float>("min_th").set(4.0f);
	conv2.parameter<float>("t_obj").set(t_obj);
	conv2.parameter<float>("lr_th").set(th_lr);
	conv2.parameter<Tensor<float>>("w").distribution<distribution::Uniform>(0.0, 1.0);
	conv2.parameter<Tensor<float>>("th").distribution<distribution::Gaussian>(5.0, 0.1);
	conv2.parameter<STDP>("stdp").set<stdp::Biological>(w_lr, 0.1);
	
	//a second pooling layer. after this layer all values become a linear vector to enter the fully connected layer.
	experiment.push_layer<layer::Pooling>("pool2", 2, 2, 2, 2);

	//the training of this layer combines all features to come up with a result 
	auto& fc1 = experiment.push_layer<layer::Convolution>("fc1", 4, 4, 256);
	fc1.parameter<float>("annealing").set(0.95f);
	fc1.parameter<float>("min_th").set(4.0f);
	fc1.parameter<float>("t_obj").set(t_obj);
	fc1.parameter<float>("lr_th").set(th_lr);
	fc1.parameter<Tensor<float>>("w").distribution<distribution::Uniform>(0.0, 1.0);
	fc1.parameter<Tensor<float>>("th").distribution<distribution::Gaussian>(5.0, 0.1);
	fc1.parameter<STDP>("stdp").set<stdp::Biological>(w_lr, 0.1);

	experiment.add_train_step(conv1, 5);
	experiment.add_train_step(conv2, 5);
	experiment.add_train_step(fc1, 5);

	conv1.plot_threshold(true);
	conv1.plot_reconstruction<process::GaussianTemporalCodingColor>(true);
	conv2.plot_reconstruction<process::GaussianTemporalCodingColor>(true);

	auto& conv1_out = experiment.output<TimeObjectiveOutput>(conv1, t_obj);
	conv1_out.add_postprocessing<process::SumPooling>(2, 2);
	conv1_out.add_postprocessing<process::FeatureScaling>();
	conv1_out.add_analysis<analysis::Activity>();
	conv1_out.add_analysis<analysis::Coherence>();
	conv1_out.add_analysis<analysis::Svm>();

	auto& conv2_out = experiment.output<TimeObjectiveOutput>(conv2, t_obj);
	conv2_out.add_postprocessing<process::SumPooling>(2, 2);
	conv2_out.add_postprocessing<process::FeatureScaling>();
	conv2_out.add_analysis<analysis::Activity>();
	conv2_out.add_analysis<analysis::Coherence>();
	conv2_out.add_analysis<analysis::Svm>();

	auto& fc1_out = experiment.output<TimeObjectiveOutput>(fc1, t_obj);
	fc1_out.add_postprocessing<process::SumPooling>(2, 2);
	fc1_out.add_postprocessing<process::FeatureScaling>();
	fc1_out.add_analysis<analysis::Activity>();
	fc1_out.add_analysis<analysis::Coherence>();
	fc1_out.add_analysis<analysis::Svm>();


	experiment.run(10000);

	return experiment.wait();
}
