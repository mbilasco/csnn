#include "Benchmark.h"
#include "process/OnOffFilter.h"
#include "process/Scaling.h"
#include "process/Pooling.h"
#include "layer/Convolution.h"
#include "layer/Pooling.h"
#include "stdp/Biological.h"
#include "analysis/Activity.h"
#include "analysis/Coherence.h"
#include "analysis/Svm.h"
#include "dataset/Mnist.h"
#include "execution/OptimizedLayerByLayer.h"
#include "stdp/BiologicalMultiplicative.h"

template<typename T>
struct Handler {
	/**
	 * @param experiment an object of type experiment, this class allows the user to add some pre-processing stages for the experiment, such as choosing the OnOffFilter, adding FeatureScaling etc..
	 * 			  in addition to choosing the experiment coding in the input layer, training and testing the datasets, and adding convolution layers, pooling layers and fully connected layers.
     * @param variables this is a mapped vector that allows adding dynamic objects to the experiment such as Time objects.
	 * @return no return (type void)
	 */
	static void run(Experiment<T>& experiment, const std::map<std::string, float>& variables) {
		//the values passed to the function add_preprocessing are (size_t filter_size, float center_dev, float surround_dev)
		experiment.template add_preprocessing<process::DefaultOnOffFilter>(7, 1.0, 4.0);
		experiment.template add_preprocessing<process::FeatureScaling>();
		//LatencyCoding function requires the input as a tensor<float> "in" and outputs the latency as time "out"
		experiment.template input<LatencyCoding>();

		//get the input path of the class, this path is needed below to choose the training sets in their file locations
		const char* input_path_ptr = std::getenv("INPUT_PATH");
		//if the input path doesn't exist, return an error message, else continue normally.
		if(input_path_ptr == nullptr) {
			throw std::runtime_error("Require to define INPUT_PATH variable");
		}
		//put the filepath in a string variable called input_path
		std::string input_path(input_path_ptr);

		//getting the data sets of faces, for training and for testing.
		experiment.template add_train<dataset::Mnist>(input_path+"train-images.idx3-ubyte", input_path+"train-labels.idx1-ubyte");
		experiment.template add_test<dataset::Mnist>(input_path+"t10k-images.idx3-ubyte", input_path+"t10k-labels.idx1-ubyte");

		//"t_obj" is a Time object - is this texpected ????????????????
		//low values of t expected lead to very local patterns, larger values lead to more global patterns
		//variables is a vector that contains variable names and their corresponding values.
		float t_obj = variables.at("t_obj");
		//there are 2 learning rates in this example, one for w and one for th.
		float w_lr = 0.1f;
		float th_lr = 1.0f;

		auto& conv1 = experiment.template push_layer<layer::Convolution>("conv1", 5, 5, 32);
		conv1.template parameter<float>("annealing").set(0.95f);
		conv1.template parameter<float>("min_th").set(1.0f);
		conv1.template parameter<float>("t_obj").set(t_obj);
		conv1.template parameter<float>("lr_th").set(th_lr);
		conv1.template parameter<Tensor<float>>("w").template distribution<distribution::Uniform>(0.0, 1.0);
		conv1.template parameter<Tensor<float>>("th").template distribution<distribution::Gaussian>(8.0, 0.1);
		conv1.template parameter<STDP>("stdp").template set<stdp::Biological>(w_lr, 0.1f);

		experiment.template push_layer<layer::Pooling>("pool1", 2, 2, 2, 2);

		auto& conv2 = experiment.template push_layer<layer::Convolution>("conv2", 5, 5, 128);
		conv2.template parameter<float>("annealing").set(0.95f);
		conv2.template parameter<float>("min_th").set(1.0f);
		conv2.template parameter<float>("t_obj").set(t_obj);
		conv2.template parameter<float>("lr_th").set(th_lr);
		conv2.template parameter<Tensor<float>>("w").template distribution<distribution::Uniform>(0.0, 1.0);
		conv2.template parameter<Tensor<float>>("th").template distribution<distribution::Gaussian>(10.0, 0.1);
		conv2.template parameter<STDP>("stdp").template set<stdp::Biological>(w_lr, 0.1f);

		experiment.template push_layer<layer::Pooling>("pool2", 2, 2, 2, 2);

		auto& fc1 = experiment.template push_layer<layer::Convolution>("fc1", 4, 4, 4096);
		fc1.template parameter<float>("annealing").set(0.95f);
		fc1.template parameter<float>("min_th").set(1.0f);
		fc1.template parameter<float>("t_obj").set(t_obj);
		fc1.template parameter<float>("lr_th").set(th_lr);
		fc1.template parameter<Tensor<float>>("w").template distribution<distribution::Uniform>(0.0, 1.0);
		fc1.template parameter<Tensor<float>>("th").template distribution<distribution::Gaussian>(10.0, 0.1);
		fc1.template parameter<STDP>("stdp").template set<stdp::Biological>(w_lr, 0.1f);

		//after creating the 3 layers they are given the same train step which is the epoch_number
		experiment.add_train_step(conv1, 100);
		experiment.add_train_step(conv2, 100);
		experiment.add_train_step(fc1, 100);

		//Getting the output of each layer by passing in the layer and a time object.
		auto& conv1_out = experiment.template output<TimeObjectiveOutput>(conv1, t_obj);
		conv1_out.template add_postprocessing<process::SumPooling>(2, 2);
		conv1_out.template add_postprocessing<process::FeatureScaling>();
		conv1_out.template add_analysis<analysis::Activity>();
		conv1_out.template add_analysis<analysis::Coherence>();
		conv1_out.template add_analysis<analysis::Svm>();

		auto& conv2_out = experiment.template output<TimeObjectiveOutput>(conv2, t_obj);
		conv2_out.template add_postprocessing<process::SumPooling>(2, 2);
		conv2_out.template add_postprocessing<process::FeatureScaling>();
		conv2_out.template add_analysis<analysis::Activity>();
		conv2_out.template add_analysis<analysis::Coherence>();
		conv2_out.template add_analysis<analysis::Svm>();

		//this is a fully connected layer (dense layers),neurons of a layer l(i) are connected to all the neurons of layer l(i+1) .
		auto& fc1_out = experiment.template output<TimeObjectiveOutput>(fc1, t_obj);
		fc1_out.template add_postprocessing<process::FeatureScaling>();
		fc1_out.template add_analysis<analysis::Activity>();
		fc1_out.template add_analysis<analysis::Coherence>();
		fc1_out.template add_analysis<analysis::Svm>();
	}
};

int main(int argc, char** argv) {
	//creating an object bench of type Benchmark and passing the needed parameters with the dataset name and the run_number
	Benchmark<Handler, OptimizedLayerByLayer> bench(argc, argv, "bench-mnist", 10);
	//adding a float vector of name "t_obj"
	bench.add_variable("t_obj", {0.1f, 0.2f, 0.3f, 0.4f, 0.5f, 0.6f, 0.7f, 0.8f, 0.9f});
	//the number passed in the run function is the thread_number
	bench.run(16);
}
