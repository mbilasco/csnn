# Meta
set(AM_MULTI_CONFIG "SINGLE")
# Directories and files
set(AM_CMAKE_BINARY_DIR "/home/isen/Bureau/mimi/falez-csnn-simulator/")
set(AM_CMAKE_SOURCE_DIR "/home/isen/Bureau/mimi/falez-csnn-simulator/")
set(AM_CMAKE_CURRENT_SOURCE_DIR "/home/isen/Bureau/mimi/falez-csnn-simulator/dep/libsvm/")
set(AM_CMAKE_CURRENT_BINARY_DIR "/home/isen/Bureau/mimi/falez-csnn-simulator/dep/libsvm/")
set(AM_CMAKE_INCLUDE_DIRECTORIES_PROJECT_BEFORE "")
set(AM_BUILD_DIR "/home/isen/Bureau/mimi/falez-csnn-simulator/dep/libsvm/libsvm_autogen")
set(AM_SOURCES "/home/isen/Bureau/mimi/falez-csnn-simulator/dep/libsvm/svm.cpp")
set(AM_HEADERS "")
# Qt environment
set(AM_QT_VERSION_MAJOR "4")
set(AM_QT_VERSION_MINOR "8")
set(AM_QT_MOC_EXECUTABLE "/usr/lib/x86_64-linux-gnu/qt4/bin/moc")
set(AM_QT_UIC_EXECUTABLE )
set(AM_QT_RCC_EXECUTABLE )
# MOC settings
set(AM_MOC_SKIP "/home/isen/Bureau/mimi/falez-csnn-simulator/dep/libsvm/libsvm_autogen/mocs_compilation.cpp")
set(AM_MOC_DEFINITIONS "DISPLAY_PLOT")
set(AM_MOC_INCLUDES "/home/isen/Bureau/mimi/falez-csnn-simulator/dep/libsvm;/home/isen/Bureau/mimi/falez-csnn-simulator/dep/libsvm/libsvm_autogen/include;/home/isen/Bureau/mimi/falez-csnn-simulator/include")
set(AM_MOC_OPTIONS "")
set(AM_MOC_RELAXED_MODE "FALSE")
set(AM_MOC_MACRO_NAMES "Q_OBJECT;Q_GADGET;Q_NAMESPACE")
set(AM_MOC_DEPEND_FILTERS "")
set(AM_MOC_PREDEFS_CMD )
# UIC settings
set(AM_UIC_SKIP )
set(AM_UIC_TARGET_OPTIONS )
set(AM_UIC_OPTIONS_FILES )
set(AM_UIC_OPTIONS_OPTIONS )
set(AM_UIC_SEARCH_PATHS )
# RCC settings
set(AM_RCC_SOURCES )
set(AM_RCC_BUILDS )
set(AM_RCC_OPTIONS )
set(AM_RCC_INPUTS )
# Configurations options
set(AM_CONFIG_SUFFIX_ "_")
