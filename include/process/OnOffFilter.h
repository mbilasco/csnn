#ifndef _PROCESS_ON_OFF_FILTER_H
#define _PROCESS_ON_OFF_FILTER_H

#include <iostream>
#include "Process.h"
#include "NumpyReader.h"

namespace process {

	namespace _priv {
		class OnOffFilterHelper {

		public:
			OnOffFilterHelper() = delete;

			static Tensor<float> generate_filter(size_t filter_size, float center_dev, float surround_dev);

		};
	}
	//multiple on/off filtering policies are tested.
	/*The first strategy: the coding is applied to channels computed as differences of pairs of RGB channels: red-green, green-blue, and blue-red.
	 The second strategy: three types of color channels exist - the (black-white) opponent channel (which corresponds to the grayscale image),
	 the (red-green) opponent channel, and the (yellow-blue) opponent channel. The second strategy applies on-center/off-center coding to the
	 (red-green) and (yellow-blue) (computed as 0.5 × R + 0.5 × G − B) channels.
	 ------- CHECK DETAILS OF THE EQUATIONS USED IN OnOffFilter.cpp ---------
	 */
	class DefaultOnOffFilter : public UniquePassProcess {

	public:
		DefaultOnOffFilter();
		DefaultOnOffFilter(size_t filter_size, float center_dev, float surround_dev);

		virtual Shape resize(const Shape& shape);
		virtual void process_train(const std::string& label, Tensor<float>& sample);
		virtual void process_test(const std::string& label, Tensor<float>& sample);

	private:
		void _process(Tensor<float>& in) const;

		size_t _filter_size;
		float _center_dev;
		float _surround_dev;

		size_t _width;
		size_t _height;
		size_t _depth;
		Tensor<float> _filter;
	};

	class CustomRGBOnOffFilter : public UniquePassProcess {

	public:
		CustomRGBOnOffFilter();
		CustomRGBOnOffFilter(const std::string& filename);

		virtual Shape resize(const Shape& shape);
		virtual void process_train(const std::string& label, Tensor<float>& sample);
		virtual void process_test(const std::string& label, Tensor<float>& sample);

	private:
		void _process(Tensor<float>& in) const;

		Tensor<float> _r;
		Tensor<float> _g;
		Tensor<float> _b;

		size_t _width;
		size_t _height;
	};

	class BiologicalOnOffFilter : public UniquePassProcess {

	public:
		BiologicalOnOffFilter();
		BiologicalOnOffFilter(size_t filter_size, float center_dev, float surround_dev);

		virtual Shape resize(const Shape& shape);
		virtual void process_train(const std::string& label, Tensor<float>& sample);
		virtual void process_test(const std::string& label, Tensor<float>& sample);

	private:
		void _process(Tensor<float>& in) const;

		size_t _filter_size;
		float _center_dev;
		float _surround_dev;

		size_t _width;
		size_t _height;
		size_t _depth;
		Tensor<float> _filter;
	};

}
#endif
