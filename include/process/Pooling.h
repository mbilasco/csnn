#ifndef _PROCESS_POOLING_H
#define _PROCESS_POOLING_H


#include "Process.h"

namespace process {
	/**
 	* takes an input sample and reduces its size by averaging the values of the pixels in the sample.
	*/
	class SumPooling : public UniquePassProcess {

	public:
		SumPooling();
		SumPooling(size_t target_width, size_t target_height);

		virtual Shape resize(const Shape& shape);
		virtual void process_train(const std::string& label, Tensor<float>& sample);
		virtual void process_test(const std::string& label, Tensor<float>& sample);

	private:
		void _process(Tensor<float>& sample) const;

		size_t _target_width;
		size_t _target_height;

		size_t _width;
		size_t _height;
		size_t _depth;
	};
}

#endif
/**
 * DETAILS FROM pdf:
 * If the output layer has multiple columns (i.e. l width (n) > 1 or l height (n) > 1), 
 * sum-pooling is applied over the positions of the feature maps to produce a feature vector
 * check equation (6.3).
 * If the output layer has only one column, it directly outputs vector g.
 * An SVM with a linear kernel is trained over the output training set.
*/
