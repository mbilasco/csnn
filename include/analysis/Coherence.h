#ifndef _ANALYSIS_COHERENCE_H
#define _ANALYSIS_COHERENCE_H

#include "Analysis.h"
namespace analysis {
	
	/**
	 * analysing coherence is important because the more the incoherence the less there is redundant features.
	 */
	class Coherence : public NoPassAnalysis {

	public:
		Coherence();

		virtual void resize(const Shape& shape);

		virtual void process();
	};

}

#endif
