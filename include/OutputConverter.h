#ifndef _OUTPUT_CONVERTER_H
#define _OUTPUT_CONVERTER_H

#include "ClassParameter.h"
#include "Spike.h"

class OutputConverter : public ClassParameter {

public:
	template<typename T, typename Factory>
	OutputConverter(const RegisterClassParameter<T, Factory>& registration) : ClassParameter(registration) {

	}

	virtual Tensor<float> process(const Tensor<float>& in) = 0;
};

class OutputConverterFactory : public ClassParameterFactory<OutputConverter, OutputConverterFactory> {

public:
	OutputConverterFactory() : ClassParameterFactory<OutputConverter, OutputConverterFactory>("OutputConverter") {

	}

};

class NoOutputConversion : public OutputConverter {

public:
	NoOutputConversion();

	virtual Tensor<float> process(const Tensor<float>& in);
};

class DefaultOutput : public OutputConverter {

public:
	DefaultOutput();
	DefaultOutput(Time min, Time max);

	virtual Tensor<float> process(const Tensor<float>& in);

private:
	Time _min;
	Time _max;

};

/**
 * reconstruction function, changes the values back from "Time" to "float" Tensor values.
*/
class TimeObjectiveOutput : public OutputConverter {

public:
    TimeObjectiveOutput();
	TimeObjectiveOutput(Time t_obj);

	virtual Tensor<float> process(const Tensor<float>& in);

private:
	Time _t_obj;

};

class SoftMaxOutput : public OutputConverter {

public:
	SoftMaxOutput();

	virtual Tensor<float> process(const Tensor<float>& in);
};

class WTAOutput : public OutputConverter {

public:
	WTAOutput();

	virtual Tensor<float> process(const Tensor<float>& in);
};




#endif
