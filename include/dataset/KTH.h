#ifndef _DATASET_KTH_H
#define _DATASET_KTH_H

#include <string>
#include <cassert>
#include <fstream>
#include <limits>
#include <tuple>

#include "Tensor.h"
#include "Input.h"

#define KTH_WIDTH 160
#define KTH_HEIGHT 120*5 //multiply by 5 to take 5 frames at a time.
#define KTH_DEPTH 3

namespace dataset {
	/**
	 * A helper class that manages the KTH dataset, opening, closing and managing the files.
	*/
	class KTH : public Input {

	public:
		KTH(const std::vector<std::string>& files);

		virtual bool has_next() const;
		virtual std::pair<std::string, Tensor<InputType>> next();
		virtual void reset();
		virtual void close();

		size_t size() const;
		virtual std::string to_string() const;

		virtual const Shape& shape() const;

	private:
		void check_next();

		std::vector<std::string> _files; //array or vector of files

		std::ifstream _reader;

		uint32_t _file_cursor;

		Shape _shape;

		uint8_t _next_label;
	};
}

#endif
