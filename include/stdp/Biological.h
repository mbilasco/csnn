#ifndef _STDP_BIOLOGICAL_H
#define _STDP_BIOLOGICAL_H

#include "Stdp.h"

namespace stdp {

	/**
	 * A class that inharrets from the STDP class. it has é main functions, 
	 * @function process which based on the time of the pre and post spikes updates the synaaptic weight,
	 * @function adapt_parameters which chqnges the parameters of the STDP learning rule such as alfa,
	*/
	class Biological : public STDP {

	public:
		Biological();
		Biological(float alpha, Time tau);

		virtual float process(float w, const Time pre, Time post);
		virtual void adapt_parameters(float factor);
	private:
		float _alpha;
		float _tau;
	};

}
#endif
