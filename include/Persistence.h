#ifndef _PERSISTENCE_H
#define _PERSISTENCE_H

#include <cstdint>
#include <sstream>

typedef uint8_t PersistenceType;

#define _PERSISTENCE_INT8	0x01
#define _PERSISTENCE_UINT8	0x02
#define _PERSISTENCE_INT16	0x03
#define _PERSISTENCE_UINT16 0x04
#define _PERSISTENCE_INT32	0x05
#define _PERSISTENCE_UINT32 0x06
#define _PERSISTENCE_INT64	0x07
#define _PERSISTENCE_UINT64 0x08
#define _PERSISTENCE_FLOAT	0x09
#define _PERSISTENCE_DOUBLE 0x0A

class Persistence {

public:
	Persistence() = delete;

	template<typename T>
	static constexpr typename std::enable_if<std::is_same<T, int8_t>::value, PersistenceType>::type to_indetifier() {
		return _PERSISTENCE_INT8;
	}

	template<typename T>
	static constexpr typename std::enable_if<std::is_same<T, uint8_t>::value, PersistenceType>::type to_indetifier() {
		return _PERSISTENCE_UINT8;
	}

	template<typename T>
	static constexpr typename std::enable_if<std::is_same<T, int16_t>::value, PersistenceType>::type to_indetifier() {
		return _PERSISTENCE_INT16;
	}

	template<typename T>
	static constexpr typename std::enable_if<std::is_same<T, uint16_t>::value, PersistenceType>::type to_indetifier() {
		return _PERSISTENCE_UINT16;
	}

	template<typename T>
	static constexpr typename std::enable_if<std::is_same<T, int32_t>::value, PersistenceType>::type to_indetifier() {
		return _PERSISTENCE_INT32;
	}

	template<typename T>
	static constexpr typename std::enable_if<std::is_same<T, uint32_t>::value, PersistenceType>::type to_indetifier() {
		return _PERSISTENCE_UINT32;
	}

	template<typename T>
	static constexpr typename std::enable_if<std::is_same<T, int64_t>::value, PersistenceType>::type to_indetifier() {
		return _PERSISTENCE_INT64;
	}

	template<typename T>
	static constexpr typename std::enable_if<std::is_same<T, uint64_t>::value, PersistenceType>::type to_indetifier() {
		return _PERSISTENCE_UINT64;
	}

	template<typename T>
	static constexpr typename std::enable_if<std::is_same<T, float>::value, PersistenceType>::type to_indetifier() {
		return _PERSISTENCE_FLOAT;
	}

	template<typename T>
	static constexpr typename std::enable_if<std::is_same<T, double>::value, PersistenceType>::type to_indetifier() {
		return _PERSISTENCE_DOUBLE;
	}


	template<template<typename> class T, typename Ret, typename... Args>
	static Ret call(PersistenceType type, Args&&... args) {
		switch(type) {

		case _PERSISTENCE_INT8:
			return T<int8_t>::apply(std::forward<Args>(args)...);
		case _PERSISTENCE_UINT8:
			return T<uint8_t>::apply(std::forward<Args>(args)...);
		case _PERSISTENCE_INT16:
			return T<int16_t>::apply(std::forward<Args>(args)...);
		case _PERSISTENCE_UINT16:
			return T<uint16_t>::apply(std::forward<Args>(args)...);
		case _PERSISTENCE_INT32:
			return T<int32_t>::apply(std::forward<Args>(args)...);
		case _PERSISTENCE_UINT32:
			return T<uint32_t>::apply(std::forward<Args>(args)...);
		case _PERSISTENCE_INT64:
			return T<int64_t>::apply(std::forward<Args>(args)...);
		case _PERSISTENCE_UINT64:
			return T<uint64_t>::apply(std::forward<Args>(args)...);
		case _PERSISTENCE_FLOAT:
			return T<float>::apply(std::forward<Args>(args)...);
		case _PERSISTENCE_DOUBLE:
			return T<double>::apply(std::forward<Args>(args)...);
		default:
			throw std::runtime_error("Unkown type "+std::to_string(static_cast<size_t>(type)));
		}
	}

	static void save_string(const std::string& str, std::ostream& stream) {
		uint32_t size = str.size();
		stream.write(reinterpret_cast<const char*>(&size), sizeof(uint32_t));
		stream.write(str.c_str(), size);
	}

	static std::string load_string(std::istream& stream) {
		uint32_t size = 0;
		stream.read(reinterpret_cast<char*>(&size), sizeof(uint32_t));

		std::string str(size, 0);
		stream.read(reinterpret_cast<char*>(&str[0]), size);
		return str;
	}


};

#endif
