#ifndef _SPIKE_H
#define _SPIKE_H

#include <cstdint>
#include <limits>

typedef float Time;

#define INFINITE_TIME std::numeric_limits<Time>::max()

/***
 * A spike is a structure that has coordinated and can be represented by a certain timestamp.
*/
struct Spike {

	Spike(Time p_time, uint16_t p_x, uint16_t p_y, uint16_t p_z) : x(p_x), y(p_y), z(p_z), time(p_time)  {

	}

	//coordinates of the pixel that is represented by this spike.
	uint8_t x;
	uint8_t y;
	uint16_t z;
	//the timestamp respresenting the spike that corresponds to the input pixel.
	Time time;

};


struct TimeComparator {

	bool operator()(const Spike& e1, const Spike& e2) const {
		return e1.time < e2.time;
	}
};


#endif
