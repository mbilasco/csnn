#ifndef _LAYER_POOLING_H
#define _LAYER_POOLING_H

#include "Layer.h"

namespace layer {

	class Pooling : public Layer3D {

	public:
        Pooling();
		//stride is how much the filter will move in x and y direction.
		Pooling(size_t filter_width, size_t filter_height, size_t stride_x, size_t stride_y, size_t padding_x = 0, size_t padding_y = 0);

		virtual void resize(const Shape& previous_shape, std::default_random_engine& random_generator);

		virtual void train(const std::string& label, const std::vector<Spike>& input_spike, const Tensor<Time>& input_time, std::vector<Spike>& output_spike);
		virtual void test(const std::string& label, const std::vector<Spike>& input_spike, const Tensor<Time>& input_time, std::vector<Spike>& output_spike);
		virtual Tensor<float> reconstruct(const Tensor<float>& t) const;

	private:
		void _exec(const std::vector<Spike>& input_spike, std::vector<Spike>& output_spike);

		Tensor<bool> _inh;
	};

}

#endif
