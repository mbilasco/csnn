#ifndef _STCONVOLTUION_H
#define _STCONVOLTUION_H

#include "Layer.h"
#include "Stdp.h"

#include "plot/Threshold.h"
#include "plot/Evolution.h"

namespace layer
{

class STConvolution;

namespace _priv
{

#ifdef SMID_AVX256
class STConvolutionImpl
{

public:
	STConvolutionImpl(STConvolution &model);

	void resize();
	void train(const std::vector<Spike> &input_spike, const Tensor<Time> &input_time, std::vector<Spike> &output_spike);
	void test(const std::vector<Spike> &input_spike, const Tensor<Time> &, std::vector<Spike> &output_spike);

private:
	STConvolution &_model;
	Tensor<float> _a;
	Tensor<float> _inh;
};

#else
class STConvolutionImpl
{

public:
	STConvolutionImpl(STConvolution &model);

	void resize();
	void train(const std::vector<Spike> &input_spike, const Tensor<Time> &input_time, std::vector<Spike> &output_spike);
	void test(const std::vector<Spike> &input_spike, const Tensor<Time> &, std::vector<Spike> &output_spike);

private:
	STConvolution &_model;
	Tensor<float> _a; //Sub-network ?
	Tensor<bool> _inh; //Inhibitory ?
};
#endif
} // namespace _priv
/**
 * STConvolution is a type of filtering applied on an input image, extracts certain features depending on a number of filters.
 * the features can be low level (curve, color, etc...) or more sophisticated features(eyes, nose, etc...)
 */
class STConvolution : public Layer3D
{

	friend class _priv::STConvolutionImpl;

public:
	/**
	 * Default constructor
	*/
	STConvolution();
	/**
	 * Constructor that takes width, hight and filter number and sets the rest by default to 1.
	 * So this is why we can call this constructor using only 4 parameters.
	*/
	STConvolution(size_t filter_width, size_t filter_height, size_t filter_number, size_t stride_x = 1, size_t stride_y = 1, size_t padding_x = 0, size_t padding_y = 0);

	STConvolution(const STConvolution &that) = delete;
	STConvolution &operator=(const STConvolution &that) = delete;

	virtual void resize(const Shape &previous_shape, std::default_random_engine &random_generator);

	virtual void train(const std::string &label, const std::vector<Spike> &input_spike, const Tensor<Time> &input_time, std::vector<Spike> &output_spike);
	virtual void test(const std::string &label, const std::vector<Spike> &input_spike, const Tensor<Time> &input_time, std::vector<Spike> &output_spike);
	virtual void on_epoch_end();

	virtual Tensor<float> reconstruct(const Tensor<float> &t) const;

	void plot_threshold(bool only_in_train);
	void plot_evolution(bool only_in_train);

private:
	//this variable is used later in adapting other variables,(the learning rates (i.e. η w and η th ) are decreased by a factor α)
	float _annealing;
	//minimum threashould - used for when decreasing the threshould to not go under a certain minimum during homéostasie
	float _min_th;
	//Time exprcted for the neuron to fire. changes from one dataset to the other.
	float _t_obj;
	//threshould learning rate.
	float _lr_th;

	//synaptic weights of of the network
	Tensor<float> _w;
	//threashoulds of the neurons of the network.
	Tensor<float> _th;
	//spike time dependent plasticity - the learning rule used
	STDP *_stdp;
	//input_depth example RGB images have a depth of 3 while greyscale have a depth of 1.
	size_t _input_depth;

	_priv::STConvolutionImpl _impl;
};
} // namespace layer
#endif
