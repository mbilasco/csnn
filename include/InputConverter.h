#ifndef _INPUT_CONVERTER_H
#define _INPUT_CONVERTER_H

#include "ClassParameter.h"
#include "Spike.h"

class InputConverter : public ClassParameter {

public:
	template<typename T, typename Factory>
	InputConverter(const RegisterClassParameter<T, Factory>& registration) : ClassParameter(registration) {

	}

	virtual void process(const Tensor<float> &in, Tensor<Time>& out) = 0;
};

class InputConverterFactory : public ClassParameterFactory<InputConverter, InputConverterFactory> {

public:
	InputConverterFactory() : ClassParameterFactory<InputConverter, InputConverterFactory>("InputConverter") {

	}

};

/**
 * the method that transforms the information into Time values
*/
class LatencyCoding : public InputConverter {

public:
	LatencyCoding();

	virtual void process(const Tensor<float> &in, Tensor<Time>& out);
};

class RankOrderCoding : public InputConverter {

public:
	RankOrderCoding();

	virtual void process(const Tensor<float> &in, Tensor<Time>& out);
};



#endif
