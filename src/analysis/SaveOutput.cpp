#include "analysis/SaveOutput.h"

using namespace analysis;

static RegisterClassParameter<SaveOutput, AnalysisFactory> _register("SaveOutput");
/**
 * SaveOutput class creates a file and logs the output of the experiment inside of it.
 * @return no return (type void)
 */
SaveOutput::SaveOutput() : UniquePassAnalysis(_register),
	_train_filename(), _test_filename(), _sparse(false), _writer(nullptr) {
	throw std::runtime_error("Unimplemented");
}

SaveOutput::SaveOutput(const std::string& train_filename, const std::string& test_filename, bool sparse) :
	UniquePassAnalysis(_register),
	_train_filename(train_filename), _test_filename(test_filename), _sparse(sparse), _writer(nullptr) {

}

void SaveOutput::resize(const Shape&) {

}

/**
 * before_train function creates a file using TensorWriter and declares if the data is sparse or not (there is different handeling of data writing if sparse).
 * @return no return (type void)
 */
void SaveOutput::before_train() {
	_writer = new TensorWriter(_train_filename, _sparse);
}
/**
 * process_train function saves the sample data with a specific label.
 * @param label label is of type string, meaningful name for the sample
 * @param sample sample is a tensor that contains the data as a Tensor<float>.
 * @return no return (type void)
 */
void SaveOutput::process_train(const std::string& label, const Tensor<float>& sample) {
	_writer->write(label, sample);
}
/**
 * after_train function closes the open writer.
 * @return no return (type void)
 */
void SaveOutput::after_train() {
	_writer->close();
	delete _writer;
}
/**
 * before_train function creates a file using TensorWriter and declares if the data is sparse or not (there is different handeling of data writing if sparse).
 * @return no return (type void)
 */
void SaveOutput::before_test() {
	_writer = new TensorWriter(_test_filename, _sparse);
}

/**
 * process_test function saves the sample data with a specific label.
 * @param label label is of type string, meaningful name for the sample
 * @param sample sample is a tensor that contains the data as a Tensor<float>.
 * @return no return (type void)
 */
void SaveOutput::process_test(const std::string& label, const Tensor<float>& sample) {
	_writer->write(label, sample);
}
/**
 * after_test function closes the open writer.
 * @return no return (type void)
 */
void SaveOutput::after_test() {
	_writer->close();
	delete _writer;
}
