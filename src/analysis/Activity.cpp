#include "analysis/Activity.h"
#include "Experiment.h"

using namespace analysis;

static RegisterClassParameter<Activity, AnalysisFactory> _register("Activity");

/**
 * Default Constructor
 */
Activity::Activity() : UniquePassAnalysis(_register),
	_sparsity(0), _activity(0), _quiet(0), _count(0), _size(0) {

}
/**
 * Resize function takes an object of type shape and fills the _size which is n features (number of features).
 * @param shape an object of type shape that has 2 array vectors (the dimenstion and product)
 */
void Activity::resize(const Shape& shape) {
	_size = shape.product();
}
/**
 * Before train function resets all the parameters.
 */
void Activity::before_train() {
	_reset();
	experiment().log() << "===Activity===" << std::endl;
	experiment().log() << "* train set:" << std::endl;
}

/**
 * Process train function takes a sample and processes it by calling the process function that calculates:
 * (sparcity according to equation 5.9 page 85, activity and count).
 */
void Activity::process_train(const std::string&, const Tensor<float>& sample) {
	_process(sample);
}

/**
 * After train function calls the Print function that displays the Sparsity, Active unit and Quiet units for the user.
 */
void Activity::after_train() {
	_print();
}

/**
 * Before test function that resets all parameters.
 */
void Activity::before_test() {
	_reset();
	experiment().log() << "* test set:" << std::endl;
}

void Activity::process_test(const std::string&, const Tensor<float>& sample) {
	_process(sample);
}
/**
 * After Test function calls the Print function that displays the Sparsity, Active unit and Quiet units for the user.
 */
void Activity::after_test() {
	_print();
}
/**
 * Reset function that sets all parameters to 0, (sparsity, activity, quiet etc..)
 */
void Activity::_reset() {
	_sparsity = 0;
	_activity = 0;
	_quiet = 0;
	_count = 0;
}
/**
 * Process function that calculates: (sparcity according to equation 5.9 page 85, activity and count)
 * @param sample sample is a tensor of type float.
 * @return no return (type void)
 */
void Activity::_process(const Tensor<float>& sample) {

	double c_sparsityn = 0; //to contain sum of the absolute value of the features
	double c_sparsityd = 0; //to contain sum of the features squared
	double c_activity = 0; //1 if the feature value is +ve and 0 if the feature value is -ve

	for(size_t i=0; i<_size; i++) {
		c_sparsityn += std::abs(sample.at_index(i));
		c_sparsityd += sample.at_index(i)*sample.at_index(i);
		c_activity += sample.at_index(i) > 0 ? 1 : 0;
	}
	//counts the units that have not been active (quiet units)
	if(c_activity == 0) {
		_quiet++;
	}
	//sum of the absolute value of the features/squqreroot of the sum of the absolute value of the features squared //equation (5.9) page 85
	//to avoid 0 in the denominator if c_sparsityd is zero tmp_1 = 1
	size_t tmp_1 = c_sparsityd == 0 ? 1.0 : std::max(1.0, c_sparsityn/std::sqrt(c_sparsityd));

	_sparsity += (std::sqrt(_size)-tmp_1)/(std::sqrt(_size)-1); //check the equation (5.9) page 85
	_activity += c_activity/static_cast<double>(_size);
	_count++;
}
/**
 * Print function that displays the Sparsity, Active unit and Quiet units for the user.
 * @return no return (type void)
 */
void Activity::_print() {
	experiment().log() << "Sparsity: " << _sparsity/static_cast<double>(_count) << std::endl;
	experiment().log() << "Active unit: " << (_activity/static_cast<double>(_count))*100.0 << "%" << std::endl;
	experiment().log() << "Quiet: " << (static_cast<double>(_quiet)/static_cast<double>(_count))*100.0 << "%" << std::endl;
}
