#include "layer/SpatioTemporalConvolution.h"

using namespace layer;

static RegisterClassParameter<STConvolution, LayerFactory> _register("STConvolution");
/**
 * STConvolution is a type of filter applied on an input immage,
 * @param annealing used as a faactor ro update other paremeteers later
 * @param min_th minimum threashould of a neuron
 * @param t_obj t expected, time expeected for a neuron to fire
 * @param lr_th learning rate of the neuron threshould, in order to have some kind of homeostasious the threshould of the neurons are updated using a certyian delta threshould.
 * @param w synapctic weights
 * @param stdp learning rule - spike time dependant plasticity
 */
STConvolution::STConvolution() : Layer3D(_register),
	_annealing(1.0), _min_th(0),_t_obj(0), _lr_th(0),
	_w(), _th(), _stdp(nullptr), _input_depth(0), _impl(*this) {

	add_parameter("annealing", _annealing, 1.0f); //Simulated annealing is a method for solving unconstrained and bound-constrained optimization problems. this parameter is used to modify other parameters
    add_parameter("min_th", _min_th); //minimum threashould
    add_parameter("t_obj", _t_obj); //time object
    add_parameter("lr_th", _lr_th); //delta threshould

    add_parameter("w", _w);  //synaptic weights
    add_parameter("th", _th); //internal threashould of neuron

    add_parameter("stdp", _stdp); //learning rule - spike time dependant plasticity
}


STConvolution::STConvolution(size_t filter_width, size_t filter_height, size_t filter_number, size_t stride_x, size_t stride_y, size_t padding_x, size_t padding_y) :
	Layer3D(_register, filter_width, filter_height, filter_number, stride_x, stride_y, padding_x, padding_y),
	_annealing(1.0), _min_th(0),_t_obj(0), _lr_th(0),
	_w(), _th(), _stdp(nullptr), _input_depth(0), _impl(*this) {

		add_parameter("annealing", _annealing, 1.0f);
		add_parameter("min_th", _min_th);
    	add_parameter("t_obj", _t_obj);
    	add_parameter("lr_th", _lr_th);

    	add_parameter("w", _w);
    	add_parameter("th", _th);

    	add_parameter("stdp", _stdp);
}

/**
 * the filter number is supposed to be equaal to the number of kernels, each filter can pick up 1 feature.
 * */
void STConvolution::resize(const Shape& previous_shape, std::default_random_engine& random_generator) {
		 Layer3D::resize(previous_shape, random_generator);

	_input_depth = previous_shape.dim(2);

	//weights filled.
	parameter<Tensor<float>>("w").shape(_filter_width, _filter_height, _input_depth, _filter_number);
	//threshoulds filled.
	parameter<Tensor<float>>("th").shape(_filter_number);

	_impl.resize();
}


void STConvolution::train(const std::string&, const std::vector<Spike>& input_spike, const Tensor<Time>& input_time, std::vector<Spike>& output_spike) {
	_impl.train(input_spike, input_time, output_spike);
}


void STConvolution::test(const std::string&, const std::vector<Spike>& input_spike, const Tensor<Time>& input_time, std::vector<Spike>& output_spike) {
	_impl.test(input_spike, input_time, output_spike);
}
/**
 * function that is called at the end of each epoch.
 * the threshould learning rate is amplified and updated by the _annealing parameter qnd the STDP is adapted.
 * */
void STConvolution::on_epoch_end() {
	_lr_th *= _annealing;
	_stdp->adapt_parameters(_annealing); //_alpha *= _annealing; So mulyiplicative STDP.
}

Tensor<float> STConvolution::reconstruct(const Tensor<float>& t) const {
	size_t k = 1;

	size_t output_width = t.shape().dim(0);
	size_t output_height = t.shape().dim(1);
	size_t output_depth = t.shape().dim(2);

	Tensor<float> out(Shape({output_width*_stride_x+_filter_width-1, output_height*_stride_y+_filter_height-1, _input_depth}));
	out.fill(0);

	Tensor<float> norm(Shape({output_width*_stride_x+_filter_width-1, output_height*_stride_y+_filter_height-1, _input_depth}));
	norm.fill(0);

	for(size_t x=0; x<output_width; x++) {
		for(size_t y=0; y<output_height; y++) {

			std::vector<size_t> is;
			for(size_t z=0; z<output_depth; z++) {
				is.push_back(z);
			}

			std::sort(std::begin(is), std::end(is), [&t, x, y](size_t i1, size_t i2) {
				return t.at(x, y, i1) > t.at(x, y, i2);
			});

			for(size_t i=0; i<k; i++) {
				if(t.at(x, y, is[i]) >= 0.0) {
					for(size_t xf=0; xf<_filter_width; xf++) {
						for(size_t yf=0; yf<_filter_height; yf++) {
							for(size_t zf=0; zf<_input_depth; zf++) {
								out.at(x*_stride_x+xf, y*_stride_y+yf, zf) += _w.at(xf, yf, zf, is[i])*t.at(x, y, is[i]);
								norm.at(x*_stride_x+xf, y*_stride_y+yf, zf) += t.at(x, y, is[i]);
							}
						}
					}
				}
			}

		}
	}

	size_t s = out.shape().product();
	for(size_t i=0; i<s; i++) {
		if(norm.at_index(i) != 0)
			out.at_index(i) /= norm.at_index(i);
	}


	out.range_normalize();

	return out;
}

void STConvolution::plot_threshold(bool only_in_train) {
	add_plot<plot::Threshold>(only_in_train, _th);
}

void STConvolution::plot_evolution(bool only_in_train) {
	add_plot<plot::Evolution>(only_in_train, _w);
}

#ifdef SMID_AVX256
#include <immintrin.h>

#define AVX_256_N 8

static __m256i _generate_mask(int n) {

	int f = 0x00000000;
	int t = 0xFFFFFFFF;

	switch(n) {

	case 0:
		return _mm256_setr_epi32(f, f, f, f, f, f, f, f);
	case 1:
		return _mm256_setr_epi32(t, f, f, f, f, f, f, f);
	case 2:
		return _mm256_setr_epi32(t, t, f, f, f, f, f, f);
	case 3:
		return _mm256_setr_epi32(t, t, t, f, f, f, f, f);
	case 4:
		return _mm256_setr_epi32(t, t, t, t, f, f, f, f);
	case 5:
		return _mm256_setr_epi32(t, t, t, t, t, f, f, f);
	case 6:
		return _mm256_setr_epi32(t, t, t, t, t, t, f, f);
	case 7:
		return _mm256_setr_epi32(t, t, t, t, t, t, t, f);
	default:
		return _mm256_setr_epi32(t, t, t, t, t, t, t, t);
	}
}

_priv::STConvolutionImpl::STConvolutionImpl(STConvolution& model) : _model(model), _a(), _inh() {

}

void _priv::STConvolutionImpl::resize() {
	_a = Tensor<float>(Shape({_model.width(), _model.height(), _model.depth()}));
	_inh = Tensor<float>(Shape({_model.width(), _model.height(), _model.depth()}));
}

void _priv::STConvolutionImpl::train(const std::vector<Spike>& input_spike, const Tensor<Time>& input_time, std::vector<Spike>&) {

	size_t depth = _model.depth();

	Tensor<float>& w = _model._w;
	Tensor<float>& th = _model._th;

	size_t n = depth/AVX_256_N;
	size_t r = depth%AVX_256_N;

	__m256 __c1 = _mm256_setzero_ps();
	__m256 __c2 = _mm256_castsi256_ps( _mm256_set1_epi32(0xFFFFFFFF));

	__m256i __mask = _generate_mask(r);

	for(size_t i=0; i<n; i++) {
		_mm256_storeu_ps(_a.ptr_index(i*AVX_256_N), __c1);
	}

	if(r > 0) {
		_mm256_maskstore_ps(_a.ptr_index(n*AVX_256_N), __mask, __c1);
	}

	for(const Spike& spike : input_spike) {


		for(size_t i=0; i<n; i++) {
			__m256 __w = _mm256_loadu_ps(w.ptr(spike.x, spike.y, spike.z, i*AVX_256_N));
			__m256 __a = _mm256_loadu_ps(_a.ptr_index(i*AVX_256_N));
			__m256 __th = _mm256_loadu_ps(th.ptr_index(i*AVX_256_N));
			__a = _mm256_add_ps(__a, __w);
			_mm256_storeu_ps(_a.ptr_index(i*AVX_256_N), __a);
			__m256 __c = _mm256_cmp_ps(__a, __th, _CMP_GE_OQ);


			if(_mm256_testz_ps(__c, __c2) == 0) {

				for(size_t j=0; j<AVX_256_N; j++) {
					if(_a.at_index(i*AVX_256_N+j) > th.at_index(i*AVX_256_N+j)) {
						for(size_t z1=0; z1<depth; z1++) {
							th.at(z1) -= _model._lr_th*(spike.time-_model._t_obj);
							if(z1 != i*AVX_256_N+j) {
								th.at(z1) -= _model._lr_th/static_cast<float>(depth-1);
							}
							else {
								th.at(z1) += _model._lr_th;
							}
							th.at(z1) = std::max<float>(_model._min_th, th.at(z1));
						}


						for(size_t x=0; x<_model._filter_width; x++) {
							for(size_t y=0; y<_model._filter_height; y++) {
								for(size_t z=0; z<_model._input_depth; z++) {
									w.at(x, y, z, i*AVX_256_N+j) = _model._stdp->process(w.at(x, y, z, i*AVX_256_N+j), input_time.at(x, y, z), spike.time);
								}
							}
						}

						return;
					}
				}
			}
		}

		if(r > 0) {
			__m256 __w = _mm256_maskload_ps(w.ptr(spike.x, spike.y, spike.z, n*AVX_256_N), __mask);
			__m256 __a = _mm256_maskload_ps(_a.ptr_index(n*AVX_256_N), __mask);
			__a = _mm256_add_ps(__a, __w);
			_mm256_maskstore_ps(_a.ptr_index(n*AVX_256_N), __mask, __a);


			for(size_t j=0; j<r; j++) {
				if(_a.at_index(n*AVX_256_N+j) > th.at_index(n*AVX_256_N+j)) {
					for(size_t z1=0; z1<depth; z1++) {
						th.at(z1) -= _model._lr_th*(spike.time-_model._t_obj);
						if(z1 != n*AVX_256_N+j) {
							th.at(z1) -= _model._lr_th/static_cast<float>(depth-1);
						}
						else {
							th.at(z1) += _model._lr_th;
						}
						th.at(z1) = std::max<float>(_model._min_th, th.at(z1));
					}


					for(size_t x=0; x<_model._filter_width; x++) {
						for(size_t y=0; y<_model._filter_height; y++) {
							for(size_t z=0; z<_model._input_depth; z++) {
								w.at(x, y, z, n*AVX_256_N+j) = _model._stdp->process(w.at(x, y, z, n*AVX_256_N+j), input_time.at(x, y, z), spike.time);
							}
						}
					}
					return;
				}
			}
		}
	}
}

void _priv::STConvolutionImpl::test(const std::vector<Spike>& input_spike, const Tensor<Time>&, std::vector<Spike>& output_spike) {
	size_t depth = _model.depth();
	Tensor<float>& w = _model._w;
	Tensor<float>& th = _model._th;

	size_t n = depth/AVX_256_N;
	size_t r = depth%AVX_256_N;

	__m256 __c1 = _mm256_setzero_ps();

	uint32_t mask = 0xFFFFFFFF;


	__m256i __mask = _generate_mask(r);

	for(size_t x=0; x<_model._current_width; x++) {
		for(size_t y=0; y<_model._current_height; y++) {
			for(size_t i=0; i<n; i++) {
				_mm256_storeu_ps(_a.ptr(x, y, i*AVX_256_N), __c1);
				_mm256_storeu_ps(_inh.ptr(x, y, i*AVX_256_N), __c1);
			}


			if(r > 0) {
				_mm256_maskstore_ps(_a.ptr(x, y, n*AVX_256_N), __mask, __c1);
				_mm256_maskstore_ps(_inh.ptr(x, y, n*AVX_256_N), __mask, __c1);
			}
		}
	}

	for(const Spike& spike : input_spike) {

		std::vector<std::tuple<uint16_t, uint16_t, uint16_t, uint16_t>> output_spikes;
		_model.forward(spike.x, spike.y, output_spikes);

		for(const auto& entry : output_spikes) {
			uint16_t x = std::get<0>(entry);
			uint16_t y = std::get<1>(entry);
			uint16_t w_x = std::get<2>(entry);
			uint16_t w_y = std::get<3>(entry);

			for(size_t i=0; i<n; i++) {
				__m256 __w = _mm256_loadu_ps(w.ptr(w_x, w_y, spike.z, i*AVX_256_N));
				__m256 __a = _mm256_loadu_ps(_a.ptr(x, y, i*AVX_256_N));
				__m256 __th = _mm256_loadu_ps(th.ptr(i*AVX_256_N));
				__a = _mm256_add_ps(__a, __w);
				_mm256_storeu_ps(_a.ptr(x, y, i*AVX_256_N), __a);
				__m256 __c = _mm256_cmp_ps(__a, __th, _CMP_GE_OQ);

				__m256 __m = _mm256_loadu_ps(_inh.ptr(x, y, i*AVX_256_N));

				if(_mm256_testc_ps(__m, __c) == 0) {
					for(size_t j=0; j<AVX_256_N; j++) {
						if(_a.at(x, y, i*AVX_256_N+j) >= th.at_index(i*AVX_256_N+j) && *reinterpret_cast<uint32_t*>(_inh.ptr(x, y, i*AVX_256_N+j)) != mask) {
							output_spike.emplace_back(spike.time, x, y, i*AVX_256_N+j);
						}
					}

					_mm256_storeu_ps(_inh.ptr(x, y, i*AVX_256_N), __c);


				}

			}

			if(r > 0) {
				__m256 __w = _mm256_maskload_ps(w.ptr(w_x, w_y, spike.z, n*AVX_256_N), __mask);
				__m256 __a = _mm256_maskload_ps(_a.ptr(x, y, n*AVX_256_N), __mask);
				__m256 __th = _mm256_maskload_ps(th.ptr(n*AVX_256_N), __mask);
				__a = _mm256_add_ps(__a, __w);
				_mm256_maskstore_ps(_a.ptr(x, y, n*AVX_256_N), __mask, __a);
				__m256 __c = _mm256_cmp_ps(__a, __th, _CMP_GE_OQ);


				for(size_t j=0; j<r; j++) {
					if(_a.at(x, y, n*AVX_256_N+j) >= th.at_index(n*AVX_256_N+j) && *reinterpret_cast<uint32_t*>(_inh.ptr(x, y, n*AVX_256_N+j)) != mask) {
						output_spike.emplace_back(spike.time, x, y, n*AVX_256_N+j);
					}
				}

				_mm256_maskstore_ps(_inh.ptr(x, y, n*AVX_256_N), __mask, __c);

			}
		}
	}
}

#else
_priv::STConvolutionImpl::STConvolutionImpl(STConvolution& model) : _model(model), _a(), _inh() {

}

void _priv::STConvolutionImpl::resize() {
	_a = Tensor<float>(Shape({_model.width(), _model.height(), _model.depth()}));
	_inh = Tensor<bool>(Shape({_model.width(), _model.height(), _model.depth()}));
}

/**
 * Very Important to read and understand - the equation of STConvolution. 
 * how weights are updated.
 * */
void _priv::STConvolutionImpl::train(const std::vector<Spike>& input_spike, const Tensor<Time>& input_time,
								   std::vector<Spike>& output_spike) {
	// gets the model depth which is in fact the filter_number. training over all the filters.
	size_t depth = _model.depth();
	//corresponds to the time dimenstion, "frames of a video".
	size_t time = _model.time();
	//fills all synaptic weights in a tensor
	Tensor<float>& w = _model._w;
	//fills all threshoulds in another tensor
	Tensor<float>& th = _model._th;

	std::fill(std::begin(_a), std::end(_a), 0);

	for(const Spike& spike : input_spike) {

		//this depth is in fact the "NUMBER OF FILTERS", FILLED IN THE RESIZE FUNCTION.
		for(size_t z=0; z<depth; z++) {
			//integrate the new value (multiple spikes are integrated to surpass the internal threshould of the neuron)
			_a.at(0, 0, z) += w.at(spike.x, spike.y, spike.z, z);
			//if the threashould is reached, enter the loop else, integrate one more value.
			if(_a.at(0, 0, z) >= th.at(z)) {
				for(size_t z1=0; z1<depth; z1++) {
					//threshould is updated according to the learning rate (_lr_th). and spike.time-_model._t_obj is infact the value of error,
					// since it's the spike time - the objectiove time the neuron should fire at. HOMEOSTASIS
					th.at(z1) -= _model._lr_th*(spike.time-_model._t_obj); //THRESHOULD DECREASED.
					if(z1 != z) { //if not the neorun who fires - threshould is decreased.
						th.at(z1) -= _model._lr_th/static_cast<float>(depth-1); 
					}
					else { // if the neuron whoi fired- increase threshould.
						th.at(z1) += _model._lr_th;
					}
					// make sure the new threshould doesn't decrease less than a required minimum.
					th.at(z1) = std::max<float>(_model._min_th, th.at(z1));
				}

				for(size_t x=0; x<_model._filter_width; x++) {
					for(size_t y=0; y<_model._filter_height; y++) {
						for(size_t zi=0; zi<_model._input_depth; zi++) {
							//finally update the synaptic weight according to STDP.
							w.at(x, y, zi, z) = _model._stdp->process(w.at(x, y, zi, z), input_time.at(x, y, zi), spike.time);
						}
					}
				}

				return;
			}
		}
	}
}

void _priv::STConvolutionImpl::test(const std::vector<Spike>& input_spike, const Tensor<Time>&, std::vector<Spike>& output_spike) {
	size_t depth = _model.depth();
	Tensor<float>& w = _model._w; //loading synaptic weights.
	Tensor<float>& th = _model._th; //loading threashoulds.

	std::fill(std::begin(_a), std::end(_a), 0);
	std::fill(std::begin(_inh), std::end(_inh), false);

	for(const Spike& spike : input_spike) {

		std::vector<std::tuple<uint16_t, uint16_t, uint16_t, uint16_t>> output_spikes;
		_model.forward(spike.x, spike.y, output_spikes);

		for(const auto& entry : output_spikes) {
			uint16_t x = std::get<0>(entry);
			uint16_t y = std::get<1>(entry);
			uint16_t w_x = std::get<2>(entry);
			uint16_t w_y = std::get<3>(entry);


			for(size_t z=0; z<depth; z++) {
				//if there is inhibition applied to this neuron go to the next one.
				if(_inh.at(x, y, z)) {
					continue;
				}

				_a.at(x, y, z) += w.at(w_x, w_y, spike.z, z);
				if(_a.at(x, y, z) >= th.at(z)) {
					output_spike.emplace_back(spike.time, x, y, z);
					_inh.at(x, y, z) = true;
				}
			}
		}
	}
}

#endif
