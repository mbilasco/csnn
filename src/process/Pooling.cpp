#include "process/Pooling.h"

using namespace process;

static RegisterClassParameter<SumPooling, ProcessFactory> _register("SumPooling");

/**
 * SumPooling is a type of pooling usually used in this project for flattening. 
 * a certain filter with a certain width and hight is taken and the output is the sum of the pixels of the input sample traversed by the filter.
*/
SumPooling::SumPooling() : UniquePassProcess(_register),
	_target_width(0), _target_height(0), _width(0), _height(0), _depth(0) {
	add_parameter("width", _target_width);
	add_parameter("height", _target_height);
}

SumPooling::SumPooling(size_t target_width, size_t target_height) : SumPooling() {
	parameter<size_t>("width").set(target_width);
	parameter<size_t>("height").set(target_height);
}

Shape SumPooling::resize(const Shape& shape) {
	_width = shape.dim(0);
	_height = shape.dim(1);
	_depth = shape.dim(2);
	return Shape({std::min<size_t>(_target_width, _width),
				  std::min<size_t>(_target_height, _height),
				  _depth});
}

void SumPooling::process_train(const std::string&, Tensor<float>& sample) {
	_process(sample);
}

void SumPooling::process_test(const std::string&, Tensor<float>& sample) {
	_process(sample);
}

/**
 * takes an input tensor and reduces its size to the target width and target hight (passed in the constructor).
*/
void SumPooling::_process(Tensor<float>& in) const {

	size_t output_width = std::min<size_t>(_target_width, _width);
	size_t output_height = std::min<size_t>(_target_height, _height);

	size_t filter_width = _width/output_width;
	size_t filter_height = _height/output_height;
	//declaring an output tensor to record the new pooled values.
	Tensor<float> out(Shape({output_width, output_height, _depth}));

	for(size_t x=0; x<output_width; x++) {
		for(size_t y=0; y<output_height; y++) {
			for(size_t z=0; z<_depth; z++) {
				float v = 0; //temporary variable to fill the output tensor according to the below equation.

				for(size_t fx=0; fx<filter_width; fx++) {
					for(size_t fy=0; fy<filter_height; fy++) {
						v += in.at(x*filter_width+fx, y*filter_height+fy, z);
					}
				}

				out.at(x, y, z) = v;
			}
		}
	}

	in = out;
}
