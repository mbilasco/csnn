#include "InputConverter.h"

//
//	LatencyCoding
//

static RegisterClassParameter<LatencyCoding, InputConverterFactory> _register_latency("LatencyCoding");

LatencyCoding::LatencyCoding() : InputConverter(_register_latency) {

}
//Tensor<float>& in is the input image as tensor and the float values are the pixels,
//the out vector is of type Time and is later used in the spike converter????????????????????
void LatencyCoding::process(const Tensor<float>&in, Tensor<Time>& out) {
	size_t size = in.shape().product();// looping all of the input tensor (image) to save the pixels; product X*Y*Z.
	//ts represents the time of the spike,
	//f in (x) = (1.0 − x) × t exposition in the PDF equation but t expposition = 1. in.at_index(i) is probably a fraction input sample/255
	for(size_t i=0; i<size; i++) {
		Time ts = std::max<Time>(0.0f, 1.0f-in.at_index(i)); //limits the input time
		out.at_index(i) = ts == 1.0f ? INFINITE_TIME : ts; //if ts == 1 then the input value was zero then we consider no spikes so infinite time
	}
}


//
//	RankOrderCoding
//

static RegisterClassParameter<RankOrderCoding, InputConverterFactory> _register_rank_order("RankOrderCoding");

RankOrderCoding::RankOrderCoding() : InputConverter(_register_rank_order) {

}

void RankOrderCoding::process(const Tensor<float>&in, Tensor<Time>& out) {
	size_t size = in.shape().product();

	std::vector<std::pair<size_t, float>> list;

	for(size_t i=0; i<size; i++) {
		list.emplace_back(i, in.at_index(i));
	}

	std::sort(std::begin(list), std::end(list), [](const auto& e1, const auto& e2) {
		return e1.second > e2.second;
	});

	for(size_t i=0; i<list.size(); i++) {
		out.at_index(list[i].first) = static_cast<Time>(i)/static_cast<Time>(list.size()-1);
	}

}
