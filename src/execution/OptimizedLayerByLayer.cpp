#include "execution/OptimizedLayerByLayer.h"

/**
 * OptimizedLayerByLayer In this class the layer training is logged in the terminal eepoch by epoch.
*/
OptimizedLayerByLayer::OptimizedLayerByLayer(ExperimentType& experiment) :
	_experiment(experiment), _train_set(), _test_set(), _current_input(), _current_output(),
	_rc_width(0), _rc_height(0), _rc_depth(0),_current_layer(0) {

}

void OptimizedLayerByLayer::process(size_t refresh_interval) {

	/**
	 * is_sorted_c & only_unique_c insure that the layers are trained in the correct order.
	*/
	bool is_sorted_c = std::is_sorted(std::begin(_experiment.train_step()), std::end(_experiment.train_step()), [](const auto& e1, const auto& e2) {
		return e1.first < e2.first;
	});

	bool only_unique_c = std::adjacent_find(std::begin(_experiment.train_step()), std::end(_experiment.train_step()), [](const auto& e1, const auto& e2) {
		return e1.first == e2.first;
	}) == std::end(_experiment.train_step());



	if(!is_sorted_c || !only_unique_c) {
		throw std::runtime_error("Layers need to be trained in right order");
	}

	_load_data();

	size_t sample_count = 0;

	size_t train_step_index = 0;

	std::vector<size_t> train_index;
	for(size_t i=0; i<_train_set.size(); i++) {
		train_index.push_back(i);
	}
	//for each layer do the following.
	for(size_t i=0; i<_experiment.layer_count(); i++) {

		_prepare(i);

		_current_layer = i;

		_experiment.print() << "Process layer " << _experiment.layer_at(i).name() << std::endl;


		if(train_step_index < _experiment.train_step().size() && _experiment.train_step()[train_step_index].first < i) {
			throw std::runtime_error("Bad train index");
		}
		else if(train_step_index < _experiment.train_step().size() && _experiment.train_step()[train_step_index].first == i) {

			size_t current_epoch = _experiment.train_step()[train_step_index].second;
			//looping through the epochs.
			for(size_t j=0; j<current_epoch; j++) {

				_experiment.print() << "Epoch " << (j+1) << "/" << current_epoch << std::endl; //display at which epoch.

				std::shuffle(std::begin(train_index), std::end(train_index), _experiment.random_generator());

				 _experiment.layer_at(i).on_epoch_start();

				for(size_t k=0; k<_train_set.size(); k++) {

					_process_sample(_train_set[train_index[k]].first, _train_set[train_index[k]].second, i);

					_experiment.tick(i, k);

					if(sample_count % refresh_interval == 0) {
						_experiment.print() << "Train layer " << _experiment.layer_at(i).name() << ", Epoch " << (j+1) << "/" << current_epoch << ", Sample " << (k+1) << "/" << _train_set.size() << std::endl;
						_experiment.refresh(i);
					}

					sample_count++;

				}
				_experiment.layer_at(i).on_epoch_end();
				_experiment.epoch(i, j);
			}
			train_step_index++;
		}

		_update_data(i, refresh_interval);
	}
}

Tensor<Time> OptimizedLayerByLayer::compute_time_at(size_t i) const {
	Tensor<Time> out(Shape({_experiment.layer_at(_current_layer).width(), _experiment.layer_at(_current_layer).height(), _experiment.layer_at(_current_layer).depth()}));
	out.fill(INFINITE_TIME);

	if(i == _current_layer) {
		for(const Spike& spike : _current_input) {
			out.at(spike.x, spike.y, spike.z) = spike.time;
		}
	}
	else if(i == _current_layer+1) {
		for(const Spike& spike : _current_output) {
			out.at(spike.x, spike.y, spike.z) = spike.time;
		}
	}

	return out;
}

void OptimizedLayerByLayer::_prepare(size_t layer_target_index) {

	size_t output_width = 1;
	size_t output_height = 1;

	if(layer_target_index >= _experiment.layer_count()) {
		output_width = _experiment.layer_at(layer_target_index).width();
		output_height = _experiment.layer_at(layer_target_index).height();
	}

	auto rc = _experiment.layer_at(layer_target_index).receptive_field_of(std::pair<uint16_t, uint16_t>(output_width, output_height));
	_rc_width = rc.first;
	_rc_height = rc.second;
	_rc_depth = layer_target_index == 0 ? _experiment.input_shape().dim(2) : _experiment.layer_at(layer_target_index-1).depth();

	_experiment.layer_at(layer_target_index).set_size(1, 1);

}

void OptimizedLayerByLayer::_process_sample(const std::string& label, const std::vector<Spike>& in, size_t layer_index) {

	size_t input_width = layer_index == 0 ? _experiment.input_layer().width(): _experiment.layer_at(layer_index-1).width();
	size_t input_height = layer_index == 0 ? _experiment.input_layer().height() : _experiment.layer_at(layer_index-1).height();
	size_t input_depth = layer_index == 0 ? _experiment.input_layer().depth() : _experiment.layer_at(layer_index-1).depth();


	size_t x = 0;
	size_t y = 0;



	if(_rc_width < input_width) {
		std::uniform_int_distribution<size_t> rand_x(0, input_width-_rc_width);
		x = rand_x(_experiment.random_generator());
	}

	if(_rc_height < input_height) {
		std::uniform_int_distribution<size_t> rand_y(0, input_height-_rc_height);
		y = rand_y(_experiment.random_generator());
	}

	_current_input.clear();

	Tensor<Time> input_time(Shape({_rc_width, _rc_height, input_depth}));
	input_time.fill(INFINITE_TIME);

	for(const Spike& spike : in) {
		if(spike.x >= x && spike.x < x+_rc_width && spike.y >= y && spike.y < y+_rc_height) {
			_current_input.emplace_back(spike.time, spike.x-x, spike.y-y, spike.z);
			input_time.at(spike.x-x, spike.y-y, spike.z) = spike.time;
		}
	}



	_current_output.clear();
	_experiment.layer_at(layer_index).train(label, _current_input, input_time, _current_output);
}

void OptimizedLayerByLayer::_update_data(size_t layer_index, size_t refresh_interval) {
	_rc_width = layer_index == 0 ? _experiment.input_layer().width() : _experiment.layer_at(layer_index-1).width();
	_rc_height = layer_index == 0 ? _experiment.input_layer().height() : _experiment.layer_at(layer_index-1).height();
	_rc_depth = layer_index == 0 ? _experiment.input_layer().depth() : _experiment.layer_at(layer_index-1).depth();

	std::vector<Output*> outputs;
	for(size_t i=0; i<_experiment.output_count(); i++) {
		if(_experiment.output_at(i).index() == layer_index) {
			outputs.push_back(&_experiment.output_at(i));
		}
	}
	_experiment.layer_at(layer_index).set_size(_experiment.layer_at(layer_index).width(), _experiment.layer_at(layer_index).height());

	for(size_t i=0; i<_train_set.size(); i++) {
		std::vector<Spike> output_spike;

		Tensor<Time> input_time(Shape({_rc_width, _rc_height, _rc_depth}));
		input_time.fill(INFINITE_TIME);
		for(Spike& spike : _train_set[i].second) {
			input_time.at(spike.x, spike.y, spike.z) = spike.time;
		}



		_experiment.layer_at(layer_index).test(_train_set[i].first, _train_set[i].second, input_time, output_spike);

		std::sort(std::begin(output_spike), std::end(output_spike), TimeComparator());

		std::swap(_train_set[i].second, output_spike);
		_train_set[i].second.shrink_to_fit();

		if(i % refresh_interval == 0) {
			_experiment.print() << "Convert layer " << _experiment.layer_at(layer_index).name() << " train sample " << (i+1) << "/" << _train_set.size() << std::endl;
		}
	}

	for(size_t i=0; i<_test_set.size(); i++) {
		std::vector<Spike> output_spike;

		Tensor<Time> input_time(Shape({_rc_width, _rc_height, _rc_depth}));
		input_time.fill(INFINITE_TIME);
		for(Spike& spike : _test_set[i].second) {
			input_time.at(spike.x, spike.y, spike.z) = spike.time;
		}

		_experiment.layer_at(layer_index).test(_test_set[i].first, _test_set[i].second, input_time, output_spike);
		std::sort(std::begin(output_spike), std::end(output_spike), TimeComparator());
		std::swap(_test_set[i].second, output_spike);
		_test_set[i].second.shrink_to_fit();


		if(i % refresh_interval == 0) {
			_experiment.print() << "Convert layer " << _experiment.layer_at(layer_index).name() << " test sample " << (i+1) << "/" << _test_set.size() << std::endl;
		}
	}

	// Output
	for(Output* output : outputs) {

		Shape shape({
					_experiment.layer_at(output->index()).width(),
					_experiment.layer_at(output->index()).height(),
					_experiment.layer_at(output->index()).depth()
					});

		std::vector<std::pair<std::string, Tensor<float>>> output_train_set;
		std::vector<std::pair<std::string, Tensor<float>>> output_test_set;

		for(std::pair<std::string, std::vector<Spike>>& entry : _train_set) {
			Tensor<Time> sample(shape);
			SpikeConverter::from_spike(entry.second, sample);
			output_train_set.emplace_back(entry.first, output->converter().process(sample));
		}

		for(std::pair<std::string, std::vector<Spike>>& entry : _test_set) {
			Tensor<Time> sample(shape);
			SpikeConverter::from_spike(entry.second, sample);
			output_test_set.emplace_back(entry.first, output->converter().process(sample));
		}

		for(Process* process : output->postprocessing()) {

			_experiment.print() << "Process " << process->class_name() << std::endl;
			_process_train_data(process, output_train_set);
			_process_test_data(process, output_test_set);
		}


		for(Analysis* analysis : output->analysis()) {

			_experiment.log() << output->name() << ", analysis " << analysis->class_name() << ":" << std::endl;

			size_t n = analysis->train_pass_number();

			for(size_t i=0; i<n; i++) {
				analysis->before_train_pass(i);
				for(std::pair<std::string, Tensor<float>>& entry : output_train_set) {
					analysis->process_train_sample(entry.first, entry.second, i);
				}
				analysis->after_train_pass(i);
			}

			if(n == 0) {
				analysis->after_test();
			}
			else {
				analysis->before_test();
				for(std::pair<std::string, Tensor<float>>& entry : output_test_set) {
					analysis->process_test_sample(entry.first, entry.second);
				}
				analysis->after_test();
			}

		}

	}
}

/**
 * in this function, the train and test data are loaded, logged and processed.
*/
void OptimizedLayerByLayer::_load_data() {
	//getting the train set.
	std::vector<std::pair<std::string, Tensor<float>>> train_set;
	for(Input* input : _experiment.train_data()) {

		size_t count = 0;
		while(input->has_next()) { // every time this loop is accessed a new sample is counted.
			train_set.push_back(input->next());
			count ++;
		}
		//logging the train set.
		_experiment.log() << "Load " << count << " train samples from " << input->to_string() << std::endl;
		input->close();

	}
	//getting the test set
	std::vector<std::pair<std::string, Tensor<float>>> test_set;
	for(Input* input : _experiment.test_data()) {

		size_t count = 0;
		while(input->has_next()) {
			test_set.push_back(input->next());
			count ++;
		}
		//logginf the test set
		_experiment.log() << "Load " << count << " test samples from " << input->to_string() << std::endl;
		input->close();

	}
	//processing both the train and test sets.
	for(Process* process : _experiment.preprocessing()) {
		_experiment.print() << "Process " << process->class_name() << std::endl;

		_process_train_data(process, train_set);
		_process_test_data(process, test_set);

	}

	for(std::pair<std::string, Tensor<float>>& entry : train_set) {
		Tensor<Time> sample(entry.second.shape());
		_experiment.input_layer().converter().process(entry.second, sample);
		_train_set.emplace_back(std::piecewise_construct, std::forward_as_tuple(entry.first), std::forward_as_tuple());
		SpikeConverter::to_spike(sample, _train_set.back().second);
		_train_set.back().second.shrink_to_fit();
	}

	for(std::pair<std::string, Tensor<float>>& entry : test_set) {
		Tensor<Time> sample(entry.second.shape());
		_experiment.input_layer().converter().process(entry.second, sample);
		_test_set.emplace_back(std::piecewise_construct, std::forward_as_tuple(entry.first), std::forward_as_tuple());
		SpikeConverter::to_spike(sample, _test_set.back().second);
		_test_set.back().second.shrink_to_fit();
	}

}

void OptimizedLayerByLayer::_process_train_data(Process* process, std::vector<std::pair<std::string, Tensor<float>>>& data) {
	size_t n = process->train_pass_number();

	for(size_t i=0; i<n; i++) {
		for(std::pair<std::string, Tensor<float>>& entry : data) {
			process->process_train_sample(entry.first, entry.second, i);

			if(entry.second.shape() != process->shape()) {
				throw std::runtime_error("Unexpected shape (actual: "+entry.second.shape().to_string()+", expected: "+process->shape().to_string()+")");
			}
		}
	}
}
void OptimizedLayerByLayer::_process_test_data(Process* process, std::vector<std::pair<std::string, Tensor<float>>>& data) {
	for(std::pair<std::string, Tensor<float>>& entry : data) {
		process->process_test_sample(entry.first, entry.second);
		if(entry.second.shape() != process->shape()) {
			throw std::runtime_error("Unexpected shape (actual: "+entry.second.shape().to_string()+", expected: "+process->shape().to_string()+")");
		}
	}
}
