#include "execution/BasicLayerByLayer.h"

BasicLayerByLayer::BasicLayerByLayer(ExperimentType& experiment) :
	_experiment(experiment), _train_set(), _test_set(), _current_time() {

}

void BasicLayerByLayer::process(size_t refresh_interval) {

	_load_data();

	size_t sample_count = 0;

	for(auto& entry : _experiment.train_step()) {

		_prepare(entry.first);

		for(size_t i=0; i<entry.second; i++) {
			_experiment.layer_at(entry.first).on_epoch_start();

			for(size_t j=0; j<_train_set.size(); j++) {

				_process_sample(_train_set[j].first, _train_set[j].second, true);

				_experiment.tick(entry.first, j);

				if(sample_count % refresh_interval == 0) {
					_experiment.print() << "Train layer " << _experiment.layer_at(entry.first).name() << ", Epoch " << (i+1) << "/" << entry.second << ", Sample " << (j+1) << std::endl;
					_experiment.refresh(entry.first);
				}

				sample_count++;
				j++;
			}

			_experiment.layer_at(entry.first).on_epoch_end();
			_experiment.epoch(entry.first, i);
		}
	}

	sample_count = 0;

	_prepare(_experiment.layer_count());

	_experiment.log() << "Record train set: " << std::endl;

	for(size_t j=0; j<_train_set.size(); j++) {

		_process_sample(_train_set[j].first, _train_set[j].second, false);

		if(sample_count % refresh_interval == 0) {
			_experiment.print() << "Record train " << (j+1) << "/" << _train_set.size() << std::endl;
		}

		sample_count++;
	}

	sample_count = 0;

	_experiment.log() << "Record test set: " << std::endl;

	for(size_t j=0; j<_test_set.size(); j++) {

		_process_sample(_test_set[j].first, _test_set[j].second, false);

		if(sample_count % refresh_interval == 0) {
			_experiment.print() << "Record test " << (j+1) << "/" << _test_set.size() << std::endl;
		}

		sample_count++;
	}
}

Tensor<Time> BasicLayerByLayer::compute_time_at(size_t i) const {
	if(i < _current_time.size()) {
		return _current_time.at(i);
	}
	else {
		return Tensor<Time>();
	}
}

void BasicLayerByLayer::_prepare(size_t layer_target_index) {
	std::vector<std::pair<uint16_t, uint16_t>> rc_list;

	if(layer_target_index < _experiment.layer_count()) {
		std::pair<uint16_t, uint16_t> rc(1, 1);
		rc_list.push_back(rc);
		for(size_t i=layer_target_index+1; i>0; i--) {
			rc = _experiment.layer_at(i-1).receptive_field_of(rc);
			rc_list.push_back(rc);
		}
		std::reverse(std::begin(rc_list), std::end(rc_list));
	}
	else {
		rc_list.emplace_back(_experiment.input_shape().dim(0), _experiment.input_shape().dim(1));
		for(size_t i=0; i<_experiment.layer_count(); i++) {
			Layer& layer = _experiment.layer_at(i);
			rc_list.emplace_back(layer.width(), layer.height());
		}
	}

	if(rc_list.front().first > _experiment.input_shape().dim(0) || rc_list.front().second > _experiment.input_shape().dim(1)) {
		throw std::runtime_error("Receptive field error");
	}

	_current_time.clear();
	for(size_t i=0; i<rc_list.size(); i++) {
		if(i == 0) {
			_current_time.emplace_back(Shape({rc_list[i].first, rc_list[i].second,  _experiment.input_shape().dim(2)}));
		}
		else {
			_experiment.layer_at(i-1).set_size(rc_list[i].first, rc_list[i].second);
			_current_time.emplace_back(Shape({rc_list[i].first, rc_list[i].second, _experiment.layer_at(i-1).depth()}));
		}

	}

	std::cout << "prepare: " << std::endl;
	for(const auto& entry : _current_time) {
		std::cout << entry.shape().to_string() << std::endl;
	}
}

void BasicLayerByLayer::_process_sample(const std::string& label, const Tensor<float>& values, bool train) {

	// Apply preprocessing

	// Extract patch
	size_t input_width = _experiment.input_shape().dim(0);
	size_t input_height = _experiment.input_shape().dim(1);
	size_t rc_width = _current_time.front().shape().dim(0);
	size_t rc_height = _current_time.front().shape().dim(1);

	size_t x = 0;
	size_t y = 0;


	if(rc_width < input_width) {
		std::uniform_int_distribution<size_t> rand_x(0, input_width-rc_width);
		x = rand_x(_experiment.random_generator());
	}

	if(rc_height < input_height) {
		std::uniform_int_distribution<size_t> rand_y(0, input_height-rc_height);
		y = rand_y(_experiment.random_generator());
	}

	std::vector<Spike> input_spike;
	_convert_input(values, x, y, input_spike);

	// Process network
	size_t limit = _current_time.size()-2;

	for(size_t i=1; i<=limit; i++) {
		if(_experiment.layer_at(i-1).require_sorted()) {
			std::sort(std::begin(input_spike), std::end(input_spike), TimeComparator());
		}

		std::vector<Spike> output_spike;
		_experiment.layer_at(i-1).test(label, input_spike, _current_time[i-1], output_spike);

		input_spike.clear();

		size_t output_width = _current_time[i].shape().dim(0);
		size_t output_height = _current_time[i].shape().dim(1);

		_current_time[i].fill(INFINITE_TIME);

		for(const Spike& spike : output_spike) {
			if(spike.x < output_width && spike.y < output_height) {
				input_spike.push_back(spike);
				_current_time[i].at(spike.x, spike.y, spike.z) = spike.time;
			}
		}
	}

	if(_experiment.layer_at(limit).require_sorted()) {
		std::sort(std::begin(input_spike), std::end(input_spike), TimeComparator());
	}
	std::vector<Spike> output_spike;

	if(train) {
		_experiment.layer_at(limit).train(label, input_spike, _current_time[limit], output_spike);
	}
	else {
		_experiment.layer_at(limit).test(label, input_spike, _current_time[limit], output_spike);
	}

	size_t output_width = _current_time[limit+1].shape().dim(0);
	size_t output_height = _current_time[limit+1].shape().dim(1);

	_current_time[limit+1].fill(INFINITE_TIME);

	for(const Spike& spike : output_spike) {
		if(spike.x < output_width && spike.y < output_height) {
			_current_time[limit+1].at(spike.x, spike.y, spike.z) = spike.time;
		}
	}
}

void BasicLayerByLayer::_convert_input(const Tensor<float>& in, size_t offset_x, size_t offset_y, std::vector<Spike>& input_spikes) {
	Tensor<Time>& t = _current_time.front();

	SpikeConverter::to_spike(in, input_spikes, offset_x, offset_y, offset_x+t.shape().dim(0), offset_y+t.shape().dim(1));

	t.fill(INFINITE_TIME);
	for(const Spike& spike : input_spikes) {
		t.at(spike.x, spike.y, spike.z) = spike.time;
	}


}

void BasicLayerByLayer::_load_data() {
	std::vector<std::pair<std::string, Tensor<float>>> train_set;
	for(Input* input : _experiment.train_data()) {

		size_t count = 0;
		while(input->has_next()) {
			train_set.push_back(input->next());
			count ++;
		}

		_experiment.log() << "Load " << count << " train samples from " << input->to_string() << std::endl;
		input->close();

	}

	std::vector<std::pair<std::string, Tensor<float>>> test_set;
	for(Input* input : _experiment.test_data()) {

		size_t count = 0;
		while(input->has_next()) {
			test_set.push_back(input->next());
			count ++;
		}

		_experiment.log() << "Load " << count << " test samples from " << input->to_string() << std::endl;
		input->close();

	}

	for(Process* process : _experiment.preprocessing()) {
		_experiment.print() << "Process " << process->class_name() << std::endl;

		_process_train_data(process, train_set);
		_process_test_data(process, test_set);

	}

	for(std::pair<std::string, Tensor<float>>& entry : train_set) {
		Tensor<Time> sample(entry.second.shape());
		_experiment.input_layer().converter().process(entry.second, sample);
		_train_set.emplace_back(std::piecewise_construct, std::forward_as_tuple(entry.first), std::forward_as_tuple(sample));
	}

	for(std::pair<std::string, Tensor<float>>& entry : test_set) {
		Tensor<Time> sample(entry.second.shape());
		_experiment.input_layer().converter().process(entry.second, sample);
		_test_set.emplace_back(std::piecewise_construct, std::forward_as_tuple(entry.first), std::forward_as_tuple(sample));
	}

}

void BasicLayerByLayer::_process_train_data(Process* process, std::vector<std::pair<std::string, Tensor<float>>>& data) {
	size_t n = process->train_pass_number();

	for(size_t i=0; i<n; i++) {
		for(std::pair<std::string, Tensor<float>>& entry : data) {
			process->process_train_sample(entry.first, entry.second, i);

			if(entry.second.shape() != process->shape()) {
				throw std::runtime_error("Unexpected shape (actual: "+entry.second.shape().to_string()+", expected: "+process->shape().to_string()+")");
			}
		}
	}
}

void BasicLayerByLayer::_process_test_data(Process* process, std::vector<std::pair<std::string, Tensor<float>>>& data) {
	for(std::pair<std::string, Tensor<float>>& entry : data) {
		process->process_test_sample(entry.first, entry.second);
		if(entry.second.shape() != process->shape()) {
			throw std::runtime_error("Unexpected shape (actual: "+entry.second.shape().to_string()+", expected: "+process->shape().to_string()+")");
		}
	}
}

