#include "dataset/KTH.h"

using namespace dataset;


KTH::KTH(const std::vector<std::string>& files) :
	_files(files), _reader(files.front(), std::ios::in | std::ios::binary),
	_file_cursor(0), _shape({KTH_WIDTH, KTH_HEIGHT, KTH_DEPTH}), _next_label(0) {

	check_next();
}
//check if has next frame.
bool KTH::has_next() const {
	return _file_cursor < _files.size();
}

//Taking an input frame of 120x160 and depth of 3.
//Take 5 frames instead of 1. by multiplying KTH_HEIGHT with 5.
std::pair<std::string, Tensor<InputType>> KTH::next() {
	assert(!_reader.eof()); //checks if the end of file is reached.
	//An output containing the frame labels and data.
	std::pair<std::string, Tensor<InputType>> out(std::to_string(static_cast<size_t>(_next_label)), _shape);

	for(size_t z=0; z<KTH_DEPTH; z++) { // looping depth - z=3 in case of RGB
		for(size_t y=0; y<KTH_HEIGHT; y++) { // looping height pixels
			for(size_t x=0; x<KTH_WIDTH; x++) {// looping width
				uint8_t pixel;
				_reader.read((char*)&pixel, sizeof(uint8_t));
				//filling all the pixel image in the output array
				out.second.at(x, y, z) = static_cast<InputType>(pixel)/static_cast<InputType>(std::numeric_limits<uint8_t>::max());
			}
		}
	}

	check_next();

	return out;
}

void KTH::reset() {
	_file_cursor = 0;// at the begining the file cursor is 0 since we still didn't start reading.
	if(_reader.is_open()) {
		_reader.close();
	}
	_reader.open(_files.front(), std::ios::in | std::ios::binary);
}

void KTH::close() {
	_reader.close();
}

size_t KTH::size() const {
	return 0;
}

//logs where are the samples from ex: KTH(/home/isen/Bureau/mimi/Workspace/Dataset_Train01.bin, ...)
std::string KTH::to_string() const {
	return "KTH("+_files.front()+", ...)";
}
const Shape& KTH::shape() const {
	return _shape;
}

//gets a new frame.
void KTH::check_next() {
	_reader.read((char*)&_next_label, sizeof(uint8_t));

	if(_reader.eof()) {
		_file_cursor++; //counts teh samples?? this is a pointer that points to the index of the files array.
		_reader.close();//if the end of file is reached, close the reader.
		if(_file_cursor < _files.size()) {
			_reader.open(_files[_file_cursor], std::ios::in | std::ios::binary);
			check_next();
		}
	}
}
