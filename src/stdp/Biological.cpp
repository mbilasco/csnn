#include "stdp/Biological.h"

using namespace stdp;

static RegisterClassParameter<Biological, STDPFactory> _register("Biological");

/**
 * there are different typess of STDP, this one is closest to the biological function.
*/
Biological::Biological() : STDP(_register), _alpha(0), _tau(0) {
	add_parameter("alpha", _alpha);
	add_parameter("tau", _tau);
}


Biological::Biological(float alpha, float tau) : Biological() {
	parameter<float>("alpha").set(alpha);
	parameter<float>("tau").set(tau);
}
/***
 * stdp rule - spike time dependant plasticity, in this equation the synaptic weights are updated according to the timestamps of the pre and post synaptic spikes.
 * if the neuron fires a spike before reciving, post time is less than pre, post is earlier than pre. w is decreased.
 * and vice versa
*/
float Biological::process(float w, const Time pre, Time post) {
	float v = pre <= post ? w+_alpha*std::exp(-(post-pre)/_tau) :  w-_alpha*std::exp(-(pre-post)/_tau);
	return std::max<float>(0, std::min<float>(1, v));
}
/**
 * Multiplicative adaptor function,
 * the value alfa helps converge to a stable state while training.
 * @param factor basically the annealing factor, this is used to increase the value of alfa in a multiplicative manner
 */
void Biological::adapt_parameters(float factor) {
	_alpha *= factor;
}
