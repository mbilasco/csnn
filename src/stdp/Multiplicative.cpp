#include "stdp/Multiplicative.h"

using namespace stdp;

static RegisterClassParameter<Multiplicative, STDPFactory> _register("Multiplicative");

Multiplicative::Multiplicative() : STDP(_register), _alpha(0), _beta(0) {
    add_parameter("alpha", _alpha); //annealing factor α that decreases the rates (ηw/lr_w and ηth:lr_th )
    add_parameter("beta", _beta); //β the parameter which controls the saturation effect.
}


Multiplicative::Multiplicative(float alpha, float beta) : Multiplicative() {
    parameter<float>("alpha").set(alpha);
    parameter<float>("beta").set(beta); //β the parameter which controls the saturation effect.
}
//pre and post are Post-synaptic and Pre-synaptic timestamps
float Multiplicative::process(float w, const Time pre, Time post) {
    //if the neuron fires a spike before reciving, post time is less than pre, post is earlier than pre. w is decreased.
    //and vice versa
	float v = pre <= post ? w+_alpha*std::exp(-_beta*w) :  w-_alpha*std::exp(_beta*(w-1.0f));
	return std::max<float>(0, std::min<float>(1, v));// to insure a value between 0 & 1.
}

//updating alpha with the annealing effect.
void Multiplicative::adapt_parameters(float factor) {
	_alpha *= factor;
}
